import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { loadState } from '@/utils/asyncStorage';
import { setLoginData } from '@/redux/ducks/userDuck';
import { loadCart } from '@/redux/ducks/cartDuck';

const StateLoader: React.FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    (async () => {
      const loginState = await loadState('login');
      const cartState = await loadState('cart');

      dispatch(setLoginData(loginState));
      dispatch(loadCart(cartState));
    })();
  }, []);

  return <></>;
};

export default StateLoader;
