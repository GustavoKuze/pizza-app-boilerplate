import { Layout } from '@ui-kitten/components';
import styled from 'styled-components/native';

export const Body = styled(Layout)`
  flex: 1;
  padding-horizontal: 16px;
  padding-top: 16px;
`;

export default Body;
