/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import {
  CarouselImage,
  CarouselTab,
  CarouselDot,
  CarouselDotTouchable,
} from './styles';
import { ViewPager, useTheme } from '@ui-kitten/components';
import CenteredView from '@sc/CenteredView';
import { TouchableWithoutFeedback } from 'react-native';

type Props = {
  navigation: {
    navigate(route: string, params?: any);
  };
  items: any[];
  isLoading: boolean;
};

const Carousel: React.FC<Props> = ({ navigation, items, isLoading }) => {
  const theme = useTheme();

  const [selectedIndex, setSelectedIndex] = useState(0);
  const [currentTimeout, setCurrentTimeout] = useState(setTimeout(() => {}, 1));

  useEffect(() => {
    clearTimeout(currentTimeout);
    setCurrentTimeout(
      setTimeout(() => {
        const index = selectedIndex + 1 >= items.length ? 0 : selectedIndex + 1;
        setSelectedIndex(index);
      }, 5000),
    );
  }, [selectedIndex, items]);

  const renderDots = () => {
    return items.map((_, i) => (
      <CarouselDotTouchable onPress={() => setSelectedIndex(i)}>
        <CarouselDot
          backgroundColor={
            theme[`color-${i === selectedIndex ? 'primary' : 'basic'}-500`]
          }
        />
      </CarouselDotTouchable>
    ));
  };

  const parseAction = (action) => {
    if (!action) {
      return;
    }

    try {
      const obj = JSON.parse(action);
      const [actionKey, actionValue] = Object.entries(obj)[0];

      switch (actionKey) {
        // @ts-ignore
        case 'product':
          navigation.navigate('ProductDetails', { productId: actionValue });
          return;
        default:
          return;
      }
    } catch (error) {}
  };

  return (
    <>
      {!isLoading && items.length > 0 ? (
        <>
          <ViewPager
            style={{ marginTop: 48 }}
            selectedIndex={selectedIndex}
            onSelect={(index) => setSelectedIndex(index)}>
            {items.map((item) => (
              <TouchableWithoutFeedback
                onPress={() => {
                  const { action } = item;
                  parseAction(action);
                }}>
                <CarouselTab key={`carousel-item-${item.id}`}>
                  <CarouselImage source={{ uri: item.image }} />
                </CarouselTab>
              </TouchableWithoutFeedback>
            ))}
          </ViewPager>
          <CenteredView row>{renderDots()}</CenteredView>
        </>
      ) : undefined}
    </>
  );
};

export { Carousel };
