import { Avatar, Layout } from '@ui-kitten/components';
import { Dimensions } from 'react-native';
import styled from 'styled-components/native';
import { TouchableOpacity } from 'react-native';

export const CarouselImage = styled(Avatar).attrs({
  size: 'medium',
  shape: 'square',
})`
  width: ${Dimensions.get('screen').width};
  height: ${Dimensions.get('screen').height / 3};
`;

export const CarouselTab = styled(Layout).attrs({ level: '2' })`
  height: 192px;
  align-items: center;
  justify-content: center;
`;

export const CarouselDot = styled(Layout)`
  height: 12px;
  width: 12px;
  border-radius: 100px;
  background-color: ${({ backgroundColor }) => backgroundColor};
`;

export const CarouselDotTouchable = styled(TouchableOpacity)`
  margin-top: 62px;
  margin-left: 12px;
  width: 32px;
  height: 32px;
  align-items: center;
  justify-content: center;
`;
