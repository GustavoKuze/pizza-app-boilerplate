/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { Avatar, Layout, Text, useTheme } from '@ui-kitten/components';
import { Dimensions, TouchableOpacity } from 'react-native';
import { CenteredView } from '@/components/StyledComponents';
import { Price } from '@/components';
import { ICartItem as ProductModel } from '@/models/ICartItem';
import { getProductFullPrice } from '@/utils';

type Props = {
  product: ProductModel;
  onPress();
};

export const Product: React.FC<Props> = ({ product, onPress }) => {
  const theme = useTheme();

  return (
    <TouchableOpacity onPress={onPress}>
      <CenteredView
        row
        extraStyles={`width: ${
          Dimensions.get('screen').width - 32
        }px; justify-content: flex-start;`}>
        <Avatar
          shape="square"
          size="giant"
          source={{ uri: product.image }}
          style={{ height: 64, width: 64 }}
        />
        <Layout style={{ marginLeft: 8 }}>
          <Text category="p1">{product.name}</Text>
          {product.variations && product.variations.length
            ? product.variations.map((variation) => (
                <Text category="p2" style={{ color: theme['color-basic-600'] }}>
                  {variation.name}: {variation.selectedOption.value}
                </Text>
              ))
            : undefined}
          <Text category="p2" style={{ color: theme['color-basic-600'] }}>
            {product.observations}
          </Text>
        </Layout>
        <Layout style={{ marginLeft: 'auto' }}>
          <CenteredView>
            <Price
              style={{ marginBottom: 16, fontSize: 18 }}
              value={getProductFullPrice(
                product,
                // @ts-ignore
                product.selectedVariationsOptions,
              )}
            />
            <Text category="p1">{product.quantity || 1}x</Text>
          </CenteredView>
        </Layout>
      </CenteredView>
    </TouchableOpacity>
  );
};
