import { Icon } from '@ui-kitten/components';
import React from 'react';
import { GestureResponderEvent, TouchableOpacity } from 'react-native';

interface Props {
  name: string;
  onPress: (event: GestureResponderEvent) => void;
  size?: number;
  color?: string;
  style?: any;
}

export const TouchableIcon: React.FC<Props> = ({
  name,
  onPress,
  size = 24,
  color = 'black',
  style = {},
}) => {
  return (
    <TouchableOpacity onPress={onPress} style={style}>
      <Icon fill={color} name={name} style={{ width: size, height: size }} />
    </TouchableOpacity>
  );
};
