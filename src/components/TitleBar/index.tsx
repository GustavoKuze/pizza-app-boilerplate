import {
  TopNavigation,
  Text,
  TopNavigationAction,
} from '@ui-kitten/components';
import React from 'react';
import { colors } from '@/constants';
import { createEvaIcon } from '@/utils/layout';

import { TitleContainer, Avatar } from './styles';

interface Props {
  title: string;
  onMenu?: Function | undefined;
  onView?: Function | undefined;
  onGoBack?: Function | undefined;
  onGoForward?: Function | undefined;
  onOk?: Function | undefined;
}

export const TitleBar: React.FC<Props> = ({
  title,
  onMenu,
  onView,
  onGoBack,
  onGoForward,
  onOk,
}) => {
  const renderTitle = () => {
    return () => (
      <TitleContainer>
        {!onGoBack && (
          <Avatar size="small" source={require('@/assets/logo.png')} />
        )}

        <Text category="h6">{title}</Text>
      </TitleContainer>
    );
  };

  const renderAccessoryLeft = () => {
    if (!onGoBack) {
      return undefined;
    }

    let icon = 'arrow-back-outline';
    let method = onGoBack;

    return () => (
      <TopNavigationAction
        icon={createEvaIcon(icon, {
          style: {
            width: 24,
            height: 24,
          },
        })}
        //@ts-ignore
        onPress={method}
      />
    );
  };

  const renderAccessoryRight = () => {
    if (!onMenu && !onGoForward && !onOk && !onView) {
      return undefined;
    }

    let icon = 'settings-2-outline';
    let method = onMenu;
    if (onOk) {
      icon = 'checkmark-outline';
      method = onOk;
    }

    if (onGoForward) {
      icon = 'arrow-forward-outline';
      method = onGoForward;
    }

    if (onView) {
      icon = 'eye-outline';
      method = onView;
    }

    return () => (
      <TopNavigationAction
        icon={createEvaIcon(icon, {
          style: {
            tintColor: onGoForward
              ? colors.info
              : onOk
              ? colors.success
              : 'black',
            width: onOk ? 32 : 24,
            height: onOk ? 32 : 24,
          },
        })}
        //@ts-ignore
        onPress={method}
      />
    );
  };

  return (
    <TopNavigation
      title={renderTitle()}
      accessoryRight={renderAccessoryRight()}
      accessoryLeft={renderAccessoryLeft()}
    />
  );
};
