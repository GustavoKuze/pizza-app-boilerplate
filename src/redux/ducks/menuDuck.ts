const typesPrefix = '@menu';

export const types = {
  SET_LOADING: `${typesPrefix}/SET_LOADING`,
  GET_MENU_ASYNC: `${typesPrefix}/GET_MENU_ASYNC`,
  SET_CATEGORIES: `${typesPrefix}/SET_CATEGORIES`,
};

const INITIAL_STATE: {
  categories: any[];
  isLoading: Boolean;
} = {
  categories: [],
  isLoading: false,
};

export default (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case types.SET_CATEGORIES:
      return { ...state, categories: action.payload };
    case types.SET_LOADING:
      return { ...state, isLoading: action.payload };
    default:
      return state;
  }
};

export const setCategories = (categories: any[]) => ({
  type: types.SET_CATEGORIES,
  payload: categories,
});

export const getMenuAsync = (onSuccess = () => {}, onError = () => {}) => ({
  type: types.GET_MENU_ASYNC,
  payload: {
    onSuccess,
    onError,
  },
});

export const setIsLoading = (isLoading: Boolean) => ({
  type: types.SET_LOADING,
  payload: isLoading,
});

export const allActions = {
  getMenuAsync,
  setCategories,
  setIsLoading,
};
