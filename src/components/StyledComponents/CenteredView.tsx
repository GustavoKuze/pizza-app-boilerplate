import { View } from 'react-native';
import styled from 'styled-components/native';

export const CenteredView = styled(View)`
  flex-direction: ${({ row }) => (row ? 'row' : 'column')};
  align-items: center;
  justify-content: center;
  ${({ extraStyles }) => (extraStyles ? extraStyles : '')}
`;

export default CenteredView;
