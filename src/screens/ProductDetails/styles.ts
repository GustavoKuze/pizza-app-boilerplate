import { Text } from '@ui-kitten/components';
import { StyleSheet } from 'react-native';

import styled from 'styled-components/native';

export const ProductName = styled(Text).attrs({
  category: 'h5',
})`
  font-weight: bold;
  margin-top: 8px;
`;

export const ProductDescription = styled(Text).attrs({
  category: 'p1',
})``;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  select: {
    margin: 2,
    marginTop: 16,
  },
});

export default styles;
