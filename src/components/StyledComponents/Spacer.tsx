import React from 'react';
import { View } from 'react-native';

// import { Container } from './styles';

interface Props {
  height?: number;
  width?: number;
  backgroundColor?: string;
  flexGrow?: number;
  marginLeft?: number;
}

const Spacer: React.FC<Props> = ({
  flexGrow = 1,
  marginLeft = 0,
  height = 140,
  width = 140,
  backgroundColor = 'white',
}) => {
  return (
    <View style={{ height, width, backgroundColor, flexGrow, marginLeft }} />
  );
};

export default Spacer;
