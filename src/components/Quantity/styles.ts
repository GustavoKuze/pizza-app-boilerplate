import { Icon } from '@ui-kitten/components';
import { TouchableOpacity } from 'react-native';
import styled from 'styled-components/native';

export const QuantityIcon = styled(Icon)`
  width: 17px;
  height: 17px;
`;

export const QtyMinusButton = styled(TouchableOpacity)`
  background-color: ${({ backgroundColor }) => backgroundColor};
  width: 34px;
  height: 34px;
  align-items: center;
  justify-content: center;
  border-bottom-left-radius: 12px;
  border-top-left-radius: 12px;
`;

export const QtyPlusButton = styled(TouchableOpacity)`
  background-color: ${({ backgroundColor }) => backgroundColor};
  width: 34px;
  height: 34px;
  align-items: center;
  justify-content: center;
  border-bottom-right-radius: 12px;
  border-top-right-radius: 12px;
`;
