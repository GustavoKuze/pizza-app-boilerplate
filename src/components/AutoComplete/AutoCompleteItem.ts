class AutoCompleteItem {
  constructor(
    public id: number = 0,
    public title: string,
    public subtitle?: string,
    public image?: string,
  ) {}
}

export default AutoCompleteItem;
