import React from 'react';
import { Dimensions, TouchableOpacity } from 'react-native';
import { View } from 'react-native';
import { ProductVariation } from '@/models/Product';
import { Text, useTheme } from '@ui-kitten/components';
import { isDarkTheme } from '@/utils';

type Props = {
  variation: ProductVariation;
  selected: number;
  onSelect(selected: number);
  style?: any;
};

export const ButtonGroup: React.FC<Props> = ({
  variation: { options, name },
  selected,
  onSelect,
  style = {},
}) => {
  const theme = useTheme();

  return (
    <>
      <Text style={{ marginBottom: 4 }}>{name}</Text>
      <View
        style={{
          borderRadius: 12,
          borderColor: theme['color-primary-500'],
          backgroundColor: theme['color-primary-500'],
          borderWidth: 2,
          overflow: 'hidden',
          width: Dimensions.get('screen').width / 1.1,
          flexDirection: 'row',
          justifyContent: 'space-between',
          flexWrap: 'wrap',
          ...style,
        }}>
        {options.map((option, i) => (
          <TouchableOpacity
            onPress={() => onSelect(i)}
            // eslint-disable-next-line react-native/no-inline-styles
            style={{
              borderWidth: 0.5,
              borderColor: theme['color-primary-500'],
              backgroundColor:
                selected === i
                  ? theme['color-primary-500']
                  : theme['background-basic-color-2'],

              // eslint-disable-next-line prettier/prettier
                            width: (Dimensions.get('screen').width / 1.1) / 2.028,
              height: 35,
              alignItems: 'center',
              justifyContent: 'center',
              margin: 0,
            }}>
            <Text
              status={selected === i || isDarkTheme() ? 'control' : 'primary'}
              style={{ textAlign: 'center' }}>
              {option.value}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    </>
  );
};
