import { Appearance } from 'react-native';
import { generateShadow } from './generateShadow';

interface Case {
  key: any;
  value: any;
}

const switchIncludes = (array: any[] | undefined, ...cases: Case[]) => {
  for (const i in cases) {
    if (array && array.includes(cases[i].key)) {
      return cases[i].value;
    }
  }

  return null;
};

const splitArrInSubArrs = (arr, itemsPerSub = 3) =>
  arr.reduce((all, one, i) => {
    const ch = Math.floor(i / itemsPerSub);
    all[ch] = [].concat(all[ch] || [], one);
    return all;
  }, []);

const getRandomInt = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min;
};

function moveToIndex(arr, oldIndex, newIndex) {
  if (newIndex >= arr.length) {
    var k = newIndex - arr.length + 1;
    while (k--) {
      arr.push(undefined);
    }
  }
  arr.splice(newIndex, 0, arr.splice(oldIndex, 1)[0]);
  return arr;
}

function debounce(func, timeout = 300) {
  let timer;
  return (...args) => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      // @ts-ignore
      func.apply(this, args);
    }, timeout);
  };
}

const toCurrency = (value = 0) => `R$ ${value.toFixed(2).replace('.', ',')}`;

const isDarkTheme = () => {
  const colorScheme = Appearance.getColorScheme();
  return colorScheme === 'dark';
};

const getProductFullPrice = (product, selectedOptions = []) => {
  const selectedOptionsSum = selectedOptions.length
    ? // @ts-ignore
      selectedOptions.map(({ price }) => price).reduce((a, b) => a + b) || 0
    : 0;

  return product.price + selectedOptionsSum;
};

export {
  switchIncludes,
  splitArrInSubArrs,
  getRandomInt,
  moveToIndex,
  debounce,
  generateShadow,
  toCurrency,
  isDarkTheme,
  getProductFullPrice,
};
