import React from 'react';
import { Dimensions, SafeAreaView } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles from './styles';
import { TitleBar, Button } from '@/components';
import { Text, useTheme } from '@ui-kitten/components';
import { Body } from '@sc/index';
import { ScrollView } from 'react-native-gesture-handler';
import { useFields } from '@/hooks/useFields';
import { ICreditCard } from '@/models/ICreditCard';
import { createCreditCard, updateCreditCard } from '@/services/creditCard';
import { useSelector } from 'react-redux';
import useModal from '@/hooks/useModal';
import { isValidCPF, isValidCNPJ, formatCPF, formatCNPJ } from 'kuze-utils';

type RootStackParamList = {
  EditCreditCard: { creditCard: ICreditCard | undefined };
};

type Props = {
  route: RouteProp<RootStackParamList, 'EditCreditCard'>;
  navigation: StackNavigationProp<RootStackParamList, 'EditCreditCard'>;
};

export const EditCreditCard: React.FC<Props> = ({ navigation, route }) => {
  const { userId } = useSelector((state: any) => state.user);
  const { showError } = useModal();
  // @ts-ignore
  const editingCreditCard: ICreditCard = (route.params || {}).creditCard || {};
  const theme = useTheme();
  const [fields, validateFields, getFieldsValues] = useFields(
    [
      {
        name: 'number',
        defaultValue: editingCreditCard.number,
        label: 'Número do cartão',
        placeholder: 'Informe o número do cartão',
        next: 'name',
        required: true,
        keyboardType: 'numeric',
      },
      {
        name: 'name',
        defaultValue: editingCreditCard.name,
        label: 'Nome do titular (como está no cartão)',
        placeholder: 'Nome do titular (como está no cartão)',
        next: 'cpfCnpj',
        required: true,
      },
      {
        name: 'cpfCnpj',
        defaultValue: editingCreditCard.cpfCnpj,
        label: 'CPF/CNPJ',
        placeholder: 'Informe seu CPF ou CNPJ de sua empresa',
        next: 'expiration',
        validator: (str) => {
          return isValidCPF(str) || isValidCNPJ(str);
        },
        formatter: (str) => {
          if (isValidCPF(str)) {
            return formatCPF(str);
          }
          if (isValidCNPJ(str)) {
            return formatCNPJ(str);
          }
          return str;
        },
        required: true,
        keyboardType: 'numeric',
      },
      {
        name: 'expiration',
        defaultValue: editingCreditCard.expiration,
        label: 'Vencimento',
        placeholder: 'E.x: 15/21',
        next: 'cvv',
        required: true,
        keyboardType: 'numeric',
      },
      {
        name: 'cvv',
        defaultValue: editingCreditCard.cvv,
        label: 'Código Verificador (CVV)',
        placeholder: 'Informe o CVV localizado atrás do cartão',
        required: true,
        keyboardType: 'numeric',
      },
    ],
    // @ts-ignore
    () => {
      createOrUpdateCreditCard();
    },
  );

  const createOrUpdateCreditCard = async () => {
    // @ts-ignore
    if (!validateFields()) {
      return;
    }
    const {
      number,
      name,
      cpfCnpj,
      expiration,
      cvv,
      // @ts-ignore
    } = getFieldsValues();

    let result: any = { data: {} };

    if (editingCreditCard.id) {
      result = await updateCreditCard(editingCreditCard.id, {
        number,
        name,
        cpfCnpj,
        expiration,
        cvv,
        userId,
        id: editingCreditCard.id,
      });
    } else {
      // @ts-ignore
      result = await createCreditCard({
        number,
        name,
        cpfCnpj,
        expiration,
        cvv,
        userId,
      });
    }

    if (result.data.error) {
      return showError(result.data.error);
    }

    navigation.goBack();
  };

  return (
    <SafeAreaView style={styles.container}>
      <TitleBar title={''} onGoBack={() => navigation.goBack()} />
      <ScrollView>
        <Body
          style={{
            paddingBottom: 32,
            height: Dimensions.get('screen').height,
          }}>
          <Text
            category="s2"
            style={{ fontSize: 18, marginLeft: 8, marginBottom: 16 }}>
            Adicione um cartão de crédito
          </Text>

          {fields}

          {/*<Text category="label" style={{ color: theme['text-hint-color'] }}>
            Bandeira
          </Text>
           <AutoComplete
            loading={false}
            placeholder="Selecione a bandeira do cartão"
            accessoryRight={createEvaIcon('credit-card-outline')}
            style={{
              width: Dimensions.get('window').width - 32,
              marginTop: 4,
              ...generateShadow(5),
            }}
            items={[
              new AutoCompleteItem(
                1,
                'Master Card',
                '',
                'https://image.pngaaa.com/548/197548-middle.png',
              ),
              new AutoCompleteItem(
                2,
                'Visa',
                '',
                'https://cdn4.iconfinder.com/data/icons/logos-and-brands/512/363_Visa_Credit_Card_logo-512.png',
              ),
            ]}
            onChange={(text) => {}}
            onSelect={(brandName, brandItem) => {

            }}
            onSubmitEditing={(brandName, brandItem) => {

            }}
            persistSelected
          /> */}

          <Button
            onPress={createOrUpdateCreditCard}
            status="primary"
            style={{ marginHorizontal: 58, marginVertical: 32 }}>
            Salvar Cartão
          </Button>
        </Body>
      </ScrollView>
    </SafeAreaView>
  );
};
