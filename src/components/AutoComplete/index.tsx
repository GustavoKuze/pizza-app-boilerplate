/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useRef } from 'react';
import {
  Avatar,
  Text,
  AutocompleteItem,
  Spinner,
  Layout,
} from '@ui-kitten/components';
import AutoCompleteItem from './AutoCompleteItem';

import { createEvaIcon } from '@/utils/layout';
import Autocomplete from '@/components/UIKIttenRaws/ui/autocomplete/autocomplete';
import NoResults from './NoResults';
interface Props {
  onChange?: Function;
  onSelect?: Function;
  placeholder?: String;
  items: AutoCompleteItem[];
  style?: any;
  loading?: Boolean;
  autoFocus?: Boolean;
  accessoryLeft?: any;
  accessoryRight?: any;
  persistSelected?: Boolean;
  onFocus?();
  onBlur?();
  onSubmitEditing?: Function;
}

const filter = (item: AutoCompleteItem, query: string) =>
  item.title.toLowerCase().includes(query.toLowerCase());

const AutoCompleteInput: React.FC<Props> = ({
  items,
  accessoryLeft,
  accessoryRight,
  style,
  onChange,
  onSelect,
  placeholder = 'Pesquisar',
  loading = false,
  autoFocus = false,
  persistSelected = false,
  onFocus = () => {},
  onBlur = () => {},
  onSubmitEditing = () => {},
}) => {
  const [value, setValue] = React.useState<string | undefined>();
  const [data, setData] = React.useState(items);
  const inputRef = useRef();

  const setDataOrEmpty = (nextData: any[] = []) => {
    if (nextData.length === 0) {
      return setData([
        {
          id: 999999,
          title: 'Nada encontrado',
          subtitle: 'Tente pesquisar usando outras palavras',
          image: 'https://pizza.com.br/media/logo.png',
        },
      ]);
    }
    return setData(nextData);
  };

  useEffect(() => {
    setDataOrEmpty(items);
  }, [items]);

  useEffect(() => {
    if (autoFocus) {
      // @ts-ignore
      inputRef.current.focus();
    }
  }, []);

  const onSelectItem = (index: number) => {
    if (data[index].id === 999999) {
      onChangeText('');
      // @ts-ignore
      return inputRef.current.focus();
    }

    const selectedName = data[index].title;
    if (onSelect) {
      onSelect(selectedName, data[index]);
    }
    setValue(persistSelected ? selectedName : '');
  };

  const onChangeText = (query: string) => {
    if (onChange) {
      onChange(query || '');
    }
    setValue(query);
    setDataOrEmpty(items.filter((item) => filter(item, query)));
  };

  const renderOption = (
    item: AutoCompleteItem,
    index: number,
  ): React.ReactElement => {
    if (item.id === 999999) {
      return <NoResults />;
    }

    return (
      <AutocompleteItem key={index} title={item.title}>
        {item.image && (
          <Avatar
            shape="square"
            source={{ uri: item.image }}
            style={{ width: 48, height: 48, marginRight: 8 }}
          />
        )}

        <Layout style={{ flexDirection: 'column' }}>
          <Text>{item.title}</Text>
          <Text category="label">{item.subtitle}</Text>
        </Layout>
      </AutocompleteItem>
    );
  };

  return (
    // @ts-ignore
    <Autocomplete
      // @ts-ignore
      ref={inputRef}
      accessoryLeft={accessoryLeft}
      accessoryRight={
        loading
          ? () => <Spinner status="primary" />
          : accessoryRight || createEvaIcon('search')
      }
      style={{ ...(style || {}) }}
      placeholder={placeholder.toString()}
      value={value}
      onSelect={onSelectItem}
      onChangeText={onChangeText}
      onFocus={onFocus}
      onBlur={onBlur}
      onSubmitEditing={onSubmitEditing}>
      {data && data.length ? data.map(renderOption) : <></>}
    </Autocomplete>
  );
};

export { AutoCompleteItem, AutoCompleteInput };
