import { IAddress } from './IAddress';
import { ICartDeliveryFee, ICartPayment, ICartUser } from './ICart';
import { ICartItem } from './ICartItem';

type Coupon = {
  id: number;
  code: string;
  discountPercentage: number;
};
export interface Order {
  id: number;
  status: string;
  date: string;
  totalPrice: number;
  address: IAddress;
  user: ICartUser | undefined;
  deliveryFee: ICartDeliveryFee;
  items: ICartItem[];
  payment: ICartPayment;
  coupons?: Coupon[] | undefined;
}
