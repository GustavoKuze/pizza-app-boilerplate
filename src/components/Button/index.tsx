import React from 'react';
import { Button as UIKittenButton, ButtonProps } from '@ui-kitten/components';
import { StyleSheet } from 'react-native';

const Button: React.FC<ButtonProps> = ({ style, ...props }) => (
  <UIKittenButton
    {...props}
    style={{ ...(style || StyleSheet.create({})), paddingVertical: 0 }}>
    {props.children}
  </UIKittenButton>
);

export { Button };
