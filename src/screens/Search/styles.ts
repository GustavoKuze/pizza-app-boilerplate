import { Avatar } from '@ui-kitten/components';
import { Dimensions, StyleSheet } from 'react-native';
import styled from 'styled-components/native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export const SearchPlaceholderImage = styled(Avatar).attrs({
  shape: 'square',
})`
  height: ${Dimensions.get('screen').width - 80}px;
  width: ${Dimensions.get('screen').width - 80}px;
  margin-top: 32px;
`;

export default styles;
