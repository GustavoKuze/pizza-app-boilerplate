import { put, call } from 'redux-saga/effects';
import { listUserCreditCards } from '@/services/creditCard';
import { types as creditCardTypes } from '@/redux/ducks/creditCardDuck';

function* fetchCreditCards(action: any) {
  yield put({
    type: creditCardTypes.SET_LOADING,
    payload: true,
  });

  const { userId, onSuccess, onError } = action.payload;

  const result = yield call(() => listUserCreditCards(userId));

  if (!result.error) {
    yield put({
      type: creditCardTypes.SET_CREDITCARDS,
      payload: result,
    });
    if (typeof onSuccess === 'function') {
      onSuccess();
    }
  } else {
    if (typeof onError === 'function') {
      onError(result.error);
    }
  }

  yield put({
    type: creditCardTypes.SET_LOADING,
    payload: false,
  });
}

export { fetchCreditCards };
