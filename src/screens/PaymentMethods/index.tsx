import React, { useEffect, useState } from 'react';
import { listPaymentMethods } from '@/services/order';
import { Dimensions, SafeAreaView, ActivityIndicator } from 'react-native';
import styles from './styles';
import { Button, TitleBar } from '@/components';
import {
  useTheme,
  Divider,
  Text,
  Radio,
  RadioGroup,
} from '@ui-kitten/components';
import { Body } from '@sc/index';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { paymentMethodsFriendlyLabels } from '@/constants';
import { useSelector } from 'react-redux';
// import { Container } from './styles';

type RootStackParamList = {
  PaymentMethods: { onSelect?: Function | undefined };
};

type Props = {
  route: RouteProp<RootStackParamList, 'PaymentMethods'>;
  navigation: StackNavigationProp<RootStackParamList, 'PaymentMethods'>;
};

const PaymentMethods: React.FC<Props> = ({ navigation, route }) => {
  const theme = useTheme();
  const [paymentMethods, setPaymentMethods] = useState<any[]>([]);
  const [isLoadingMethods, setIsLoadingMethods] = useState(true);
  const {
    cart: { payment: cartPayment },
  } = useSelector((state: any) => state.cart);
  const [selectedIndex, setSelectedIndex] = React.useState(0);

  useEffect(() => {
    (async () => {
      setIsLoadingMethods(true);
      const methods = await listPaymentMethods();
      setPaymentMethods(methods);

      if (cartPayment.id) {
        setSelectedIndex(methods.findIndex(({ id }) => id === cartPayment.id));
      }

      setIsLoadingMethods(false);
    })();
  }, []);

  return (
    <SafeAreaView style={styles.container}>
      <TitleBar
        title={''}
        // onGoBack={
        //   typeof route.params?.onSelect !== 'function'
        //     ? () => navigation.goBack()
        //     : undefined
        // }
        onGoBack={() => navigation.goBack()}
      />

      <Divider />

      <Body style={{ paddingBottom: 32 }}>
        <Text
          category="s2"
          style={{ fontSize: 18, marginLeft: 8, marginBottom: 16 }}>
          Escolha a forma de pagamento
        </Text>

        {!isLoadingMethods && paymentMethods.length > 0 ? (
          <>
            <RadioGroup
              selectedIndex={selectedIndex}
              onChange={(index) => setSelectedIndex(index)}>
              {paymentMethods.map(({ id, name }) => (
                <Radio status="primary" key={id}>
                  <Text category="S2" style={{ fontSize: 18 }}>
                    {paymentMethodsFriendlyLabels[name]}
                  </Text>
                </Radio>
              ))}
            </RadioGroup>
            <Button
              onPress={() => {
                const paymentMethod = paymentMethods[selectedIndex];
                // @ts-ignore
                route.params?.onSelect(paymentMethod, cartPayment.card);
              }}
              status="primary"
              style={{
                marginHorizontal: 58,
                marginTop: Dimensions.get('screen').height / 3,
              }}>
              Continuar
            </Button>
          </>
        ) : (
          <ActivityIndicator
            style={{ marginTop: Dimensions.get('screen').height / 3 }}
            size={64}
            color={theme['color-primary-500']}
          />
        )}
      </Body>
    </SafeAreaView>
  );
};

export default PaymentMethods;
