/* eslint-disable react-hooks/rules-of-hooks */
import { generateShadow } from '@/utils';
import { Icon, Input } from '@ui-kitten/components';
import React, { useRef, useState } from 'react';
import { KeyboardTypeOptions, TouchableWithoutFeedback } from 'react-native';

interface Field {
  placeholder?: string;
  name: string;
  secure?: boolean;
  label?: string;
  next?: string;
  textarea?: boolean;
  required?: boolean;
  regex?: RegExp;
  // regex?: RegExp | RegExp[];
  validator?(str: string): boolean;
  formatter?(str: string): string;
  keyboardType?: KeyboardTypeOptions;
  defaultValue?: any;
}

const useFields = (fields: Field[], submit = () => {}) => {
  let inputs: any[] = [];
  let fieldsObjects: any[] = [];

  const validateFields = (): Boolean => {
    const fieldsValidations = fieldsObjects.map((field) => {
      let isValid = true;

      const fieldInitializer = fields.find(
        ({ name: fName }) => fName === field.name,
      );

      if (fieldInitializer?.required) {
        isValid = !!field.state;
      }

      if (fieldInitializer?.regex) {
        isValid = fieldInitializer.regex.test(field.state);
      }
      // if (fieldInitializer?.regex) {
      //   // @ts-ignore
      //   if (fieldInitializer?.regex.length) {
      //     let v = false;
      //     // @ts-ignore
      //     fieldInitializer?.regex.forEach((r) => {
      //       // eslint-disable-next-line curly
      //       if (!v) v = r.test(field.state);
      //     });
      //     isValid = v;
      //   } else {
      //     // @ts-ignore
      //     isValid = fieldInitializer.regex.test(field.state);
      //   }
      // }

      if (fieldInitializer?.validator) {
        isValid = fieldInitializer.validator(field.state);
      }

      field.setStatus(isValid ? 'success' : 'danger');
      return { isValid };
    });

    return fieldsValidations.filter(({ isValid }) => !isValid).length === 0;
  };

  const getFieldsValues = () => {
    let fields = {};
    fieldsObjects
      .map(({ name, state }) => ({ [`${name}`]: state }))
      .forEach((obj) => {
        fields = { ...fields, ...obj };
      });
    return fields;
  };

  const setFieldsValues = (newValues: any[]) => {
    newValues.map((nameValue) => {
      const fo = fieldsObjects.find(
        (f) => f.name === Object.entries(nameValue)[0][0],
      );
      if (fo) {
        fo.setState(Object.entries(nameValue)[0][1]);
      }
    });

    return true;
  };

  fields.map((field) => {
    const [state, setState] = useState(field.defaultValue || '');
    const ref = useRef<any>();
    const [status, setStatus] = useState('error');
    const [secureTextEntry, setSecureTextEntry] = React.useState(true);

    const toggleSecureEntry = () => {
      setSecureTextEntry(!secureTextEntry);
    };

    const renderIcon = (props) => (
      <TouchableWithoutFeedback onPress={toggleSecureEntry}>
        <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'} />
      </TouchableWithoutFeedback>
    );

    const input = (
      <Input
        ref={ref}
        placeholder={field.placeholder}
        label={field.label}
        value={state}
        multiline={!!field.textarea}
        style={{ ...generateShadow(3), marginBottom: 4 }}
        textStyle={field.textarea ? { minHeight: 64 } : undefined}
        keyboardType={field.keyboardType || 'default'}
        accessoryRight={field.secure ? renderIcon : undefined}
        secureTextEntry={field.secure ? secureTextEntry : false}
        onChangeText={(nextValue) => {
          if (field.formatter) {
            setState(field.formatter(nextValue));
          } else {
            setState(nextValue);
          }
        }}
        onBlur={validateFields}
        onSubmitEditing={() => {
          if (!field.textarea) {
            if (field.next) {
              const nextField = fieldsObjects.find(
                (obj) => obj.name === field.next,
              );

              nextField.ref.current.focus();
            } else {
              submit();
            }
          }
        }}
        status={status}
      />
    );

    inputs = [...inputs, input];

    fieldsObjects = [
      ...fieldsObjects,
      {
        name: field.name,
        ref,
        setStatus,
        state,
        setState,
        input,
      },
    ];
  });

  return [inputs, validateFields, getFieldsValues, setFieldsValues];
};

export { useFields };
