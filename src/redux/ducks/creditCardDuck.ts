const typesPrefix = '@creditCard';

export const types = {
  SET_LOADING: `${typesPrefix}/SET_LOADING`,
  GET_CREDITCARDS_ASYNC: `${typesPrefix}/GET_CREDITCARDS_ASYNC`,
  SET_CREDITCARDS: `${typesPrefix}/SET_CREDITCARDS`,
};

const INITIAL_STATE: {
  creditCards: any[];
  isLoading: Boolean;
} = {
  creditCards: [],
  isLoading: false,
};

export default (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case types.SET_CREDITCARDS:
      return { ...state, creditCards: action.payload };
    case types.SET_LOADING:
      return { ...state, isLoading: action.payload };
    default:
      return state;
  }
};

export const setCreditCards = (creditCards: any[]) => ({
  type: types.SET_CREDITCARDS,
  payload: creditCards,
});

export const getCreditCardsAsync = (
  userId: number,
  onSuccess = () => {},
  onError = () => {},
) => ({
  type: types.GET_CREDITCARDS_ASYNC,
  payload: {
    userId,
    onSuccess,
    onError,
  },
});

export const setIsLoading = (isLoading: Boolean) => ({
  type: types.SET_LOADING,
  payload: isLoading,
});

export const allActions = {
  getCreditCardsAsync,
  setCreditCards,
  setIsLoading,
};
