import React from 'react';
import { SafeAreaView } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles, { SettingsDivider } from '../styles';
import { TitleBar } from '@/components';
import { Divider, Toggle } from '@ui-kitten/components';
import { Body } from '@sc/index';
import { colors } from '@/constants';
import { useToggleState } from '@/hooks/useToggleState';
import { ToggleContainer } from './style';

type RootStackParamList = {
  ContactSettings: undefined;
};

type Props = {
  route: RouteProp<RootStackParamList, 'ContactSettings'>;
  navigation: StackNavigationProp<RootStackParamList, 'ContactSettings'>;
};

export const ContactSettings: React.FC<Props> = ({ navigation }) => {
  const receivePushState = useToggleState();
  const receiveEmailState = useToggleState();

  return (
    <SafeAreaView style={styles.container}>
      <TitleBar title={'Notificações'} onGoBack={() => navigation.goBack()} />
      <SettingsDivider />
      <Body>
        <ToggleContainer>
          <Toggle status="success" {...receivePushState}>
            Receber notificações push
          </Toggle>
        </ToggleContainer>
        <Divider style={{ backgroundColor: colors.divider, marginTop: 16 }} />
        <ToggleContainer>
          <Toggle status="success" {...receiveEmailState}>
            Receber notificações por e-mail
          </Toggle>
        </ToggleContainer>
        <Divider style={{ backgroundColor: colors.divider, marginTop: 16 }} />
      </Body>
    </SafeAreaView>
  );
};
