import { Button } from '@ui-kitten/components';
import styled from 'styled-components/native';

export const Container = styled.View``;

export const FacebookButton = styled(Button)`
  margin-top: 8px;
  height: 3px;
  padding-vertical: 0;
`;

export const GoogleButton = styled(Button)`
  margin-top: 8px;
  height: 3px;
  padding-vertical: 0;
`;
