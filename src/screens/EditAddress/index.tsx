/* eslint-disable curly */
import React, { useState } from 'react';
import { Dimensions, SafeAreaView } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles from './styles';
import {
  TitleBar,
  AutoCompleteInput as AutoComplete,
  AutoCompleteItem,
  Button,
} from '@/components';
import { Text, useTheme } from '@ui-kitten/components';
import { Body } from '@sc/index';
import { ScrollView } from 'react-native-gesture-handler';
import { useFields } from '@/hooks/useFields';
import {
  createAddress,
  getAddressByCEP,
  updateAddress,
} from '@/services/address';
import { useSelector } from 'react-redux';
import useModal from '@/hooks/useModal';
import { IAddress } from '@/models/IAddress';
import { formatCEP, isValidCEP, getStates } from 'kuze-utils';
import { generateShadow } from '@/utils';

type RootStackParamList = {
  EditAddress: { address: IAddress | undefined };
};

type Props = {
  route: RouteProp<RootStackParamList, 'EditAddress'>;
  navigation: StackNavigationProp<RootStackParamList, 'EditAddress'>;
};

export const EditAddress: React.FC<Props> = ({ navigation, route }) => {
  const { userId } = useSelector((state: any) => state.user);
  const { showError } = useModal();
  // @ts-ignore
  const editingAddress: IAddress = (route.params || {}).address || {};
  const theme = useTheme();
  const [state, setState] = useState(editingAddress.state || '');

  const [fields, validateFields, getFieldsValues, setFieldsValues] = useFields(
    [
      {
        name: 'name',
        label: 'Nome',
        placeholder: 'Dê um nome para o endereço',
        next: 'cep',
        required: false,
        defaultValue: editingAddress.name,
      },
      {
        name: 'cep',
        label: 'CEP',
        placeholder: 'Informe o CEP do endereço',
        next: 'street',
        validator: (str) => isValidCEP(str),
        formatter: (str) => {
          callGetAddressByCEP(str);
          return formatCEP(str);
        },
        required: true,
        keyboardType: 'numeric',
        defaultValue: editingAddress.cep,
      },
      {
        name: 'street',
        label: 'Rua',
        placeholder: 'E.x: Rua das Camélias',
        next: 'number',
        required: true,
        defaultValue: editingAddress.street,
      },
      {
        name: 'number',
        label: 'Número',
        placeholder: 'Número do endereço',
        next: 'neighborhood',
        required: true,
        keyboardType: 'numeric',
        defaultValue: editingAddress.number,
      },
      {
        name: 'neighborhood',
        label: 'Bairro',
        placeholder: 'Informe seu bairro',
        next: 'complement',
        required: true,
        defaultValue: editingAddress.neighborhood,
      },
      {
        name: 'complement',
        label: 'Complemento',
        placeholder: 'Casa, Apartamento, etc...',
        next: 'city',
        required: false,
        defaultValue: editingAddress.complement,
      },
      {
        name: 'city',
        label: 'Cidade',
        placeholder: 'Nome da cidade',
        required: true,
        defaultValue: editingAddress.city,
      },
    ],
    // @ts-ignore
    () => {
      createOrUpdateAddress();
    },
  );

  const createOrUpdateAddress = async () => {
    // @ts-ignore
    if (!validateFields() || !state) {
      return;
    }
    const {
      cep,
      city,
      complement,
      country,
      neighborhood,
      number,
      street,
      name,
      // @ts-ignore
    } = getFieldsValues();

    let result: any = { data: {} };

    if (editingAddress.id) {
      result = await updateAddress(editingAddress.id, {
        cep,
        city,
        complement,
        country,
        id: editingAddress.id,
        neighborhood,
        number,
        state,
        street,
        name,
        userId,
      });
    } else {
      // @ts-ignore
      result = await createAddress({
        cep,
        city,
        complement,
        country,
        neighborhood,
        number,
        state,
        street,
        name,
        userId,
      });
    }

    if (result.data.error) {
      return showError(result.data.error);
    }

    navigation.goBack();
  };

  const callGetAddressByCEP = async (cep) => {
    if (!isValidCEP(cep)) {
      return;
    }
    const { data, ok } = await getAddressByCEP(cep);
    if (ok && !data.error && data.data.cep) {
      const { state, city, neighborhood, street } = data.data;
      let newValues: any[] = [];

      if (state) setState(state);
      if (city) newValues.push({ city });
      if (neighborhood) newValues.push({ neighborhood });
      if (street) newValues.push({ street });
      // @ts-ignore
      setFieldsValues(newValues);
    }
  };

  return (
    <SafeAreaView style={styles.container}>
      <TitleBar title={''} onGoBack={() => navigation.goBack()} />
      <ScrollView>
        <Body style={{ paddingBottom: 32 }}>
          <Text
            category="s2"
            style={{ fontSize: 18, marginLeft: 8, marginBottom: 16 }}>
            Adicione um endereço
          </Text>

          {fields}

          <Text category="label" style={{ color: theme['text-hint-color'] }}>
            Estado (UF)
          </Text>
          <AutoComplete
            loading={false}
            placeholder={editingAddress.state || 'Selecione o Estado na lista'}
            style={{
              width: Dimensions.get('window').width - 32,
              marginTop: 4,
              ...generateShadow(5),
            }}
            items={getStates().map(
              ({ code }, i) => new AutoCompleteItem(i, code),
            )}
            onSelect={(s) => setState(s)}
            persistSelected
          />

          <Button
            onPress={createOrUpdateAddress}
            status="primary"
            style={{ marginHorizontal: 58, marginVertical: 32 }}>
            Salvar Endereço
          </Button>
        </Body>
      </ScrollView>
    </SafeAreaView>
  );
};
