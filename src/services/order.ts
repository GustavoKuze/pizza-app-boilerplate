import { ICart } from '@/models/ICart';
import { request } from './api';

const createOrder = async (order: ICart): Promise<any> => {
  return request('/order', 'POST', order);
};

const listPaymentMethods = async (): Promise<any[]> => {
  try {
    const response = await request('/order/payment-methods');

    return response.data.data;
  } catch (err) {
    console.log(err);
    return [];
  }
};

const listDeliveryFees = async (): Promise<any[]> => {
  try {
    const response = await request('/delivery-fee');

    return response.data.data;
  } catch (err) {
    console.log(err);
    return [];
  }
};

const isCouponValid = async (code: string): Promise<any> => {
  try {
    const response = await request(`/discount-coupon/is-valid/${code}`);

    return response.data.data;
  } catch (err) {
    console.log(err);
    return false;
  }
};

const listOrders = async (userId: number): Promise<any[]> => {
  try {
    const response = await request(`/order/${userId}`);

    return response.data.data;
  } catch (err) {
    console.log(err);
    return [];
  }
};

const getOrderDetails = async (orderId: number): Promise<any> => {
  try {
    const response = await request(`/order/details/${orderId}`);

    return response.data.data;
  } catch (err) {
    console.log(err);
    return {};
  }
};

export {
  createOrder,
  listPaymentMethods,
  listDeliveryFees,
  isCouponValid,
  listOrders,
  getOrderDetails,
};
