import { Avatar as UIKittenAvatar, Layout } from '@ui-kitten/components';
import styled from 'styled-components/native';

export const TitleContainer = styled(Layout)`
  flex-direction: row;
  align-items: center;
`;

export const Avatar = styled(UIKittenAvatar)`
  margin-horizontal: 16px;
`;
