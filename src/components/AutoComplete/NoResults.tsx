import React from 'react';
import CenteredView from '@sc/CenteredView';
import { NoResultsImage } from './styles';
import { Text, useTheme } from '@ui-kitten/components';

const NoResults = () => {
  const theme = useTheme();

  return (
    <CenteredView
      extraStyles={`
        background-color: ${theme['background-basic-color-1']};
        height: 100%;
    `}>
      <NoResultsImage
        size="giant"
        source={require('@/assets/screen_placeholder.png')}
        shape="square"
      />
      <Text category="h5" style={{ color: theme['color-primary-400'] }}>
        Nada encontrado
      </Text>
    </CenteredView>
  );
};

export default NoResults;
