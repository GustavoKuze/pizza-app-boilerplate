export interface IAddress {
  id: number;
  street: string;
  neighborhood: string;
  complement: string;
  city: string;
  state: string;
  country: string;
  number: string;
  cep: string;
  userId?: number;
  createdAt?: Date;
  updatedAt?: Date;
  name?: string;
}
