import React from 'react';
import { Dimensions, SafeAreaView } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles from './styles';
import { TitleBar } from '@/components';
import CenteredView from '@sc/CenteredView';
import { Avatar, Text, Divider } from '@ui-kitten/components';
import { colors } from '@/constants';
import { appDisplayName } from '@/config/settings.json';

type RootStackParamList = {
  About: undefined;
};

type Props = {
  route: RouteProp<RootStackParamList, 'About'>;
  navigation: StackNavigationProp<RootStackParamList, 'About'>;
};

export const About: React.FC<Props> = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <TitleBar title={'Sobre'} onGoBack={() => navigation.goBack()} />
      <CenteredView style={{ marginTop: 32 }}>
        <Avatar
          size="giant"
          style={{
            height: 160,
            width: 160,
          }}
          source={require('../../assets/logo.png')}
        />
        <Text category="h4" style={{ fontWeight: 'bold' }}>
          {appDisplayName}
        </Text>
        <Text category="h5">v{require('../../../package.json').version}</Text>
        <Divider
          style={{
            backgroundColor: colors.divider,
            marginVertical: 16,
            height: 1,
            width: Dimensions.get('window').width,
          }}
        />
        <Text category="h5">Firestorm Apps</Text>
        <Text category="h5">suporte@pizza.com.br</Text>
        <Divider
          style={{
            backgroundColor: colors.divider,
            marginVertical: 16,
            height: 1,
            width: Dimensions.get('window').width,
          }}
        />
      </CenteredView>
    </SafeAreaView>
  );
};
