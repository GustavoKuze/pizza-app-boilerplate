/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';
import { SafeAreaView, TouchableOpacity, View } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles from './styles';
import { Spinner, Text, useTheme } from '@ui-kitten/components';
import { Body } from '@sc/index';
import { useFields } from '@/hooks/useFields';
import { SocialLoginButtons, Button, AppLogo } from '@/components';
import { loginAsync } from '@/redux/ducks/userDuck';
import useModal from '@/hooks/useModal';
import { useDispatch, useSelector } from 'react-redux';
import { generateShadow } from '@/utils';

type RootStackParamList = {
  Login: undefined;
  SignUp: undefined;
  ForgetPassword: undefined;
  Home: undefined;
};

type Props = {
  route: RouteProp<RootStackParamList, 'Login'>;
  navigation: StackNavigationProp<RootStackParamList, 'Login'>;
};

export const Login: React.FC<Props> = ({ navigation }) => {
  const [fields, validateFields, getFieldsValues] = useFields(
    [
      {
        name: 'email',
        label: 'E-mail',
        placeholder: 'Informe seu melhor e-mail',
        next: 'password',
        regex: /\S+@\S+\.\S+/,
        required: true,
        keyboardType: 'email-address',
      },
      {
        name: 'password',
        label: 'Senha',
        placeholder: 'Digite uma senha forte',
        secure: true,
        required: true,
      },
    ],
    // @ts-ignore
    () => {
      callLogin();
    },
  );

  const dispatch = useDispatch();
  const { token, isLoading } = useSelector((state: any) => state.user);
  const theme = useTheme();

  const { showError } = useModal();

  useEffect(() => {
    if (token) {
      navigation.replace('Home');
    }
  }, [token]);

  const callLogin = async () => {
    // navigation.replace('Home');

    // @ts-ignore
    if (!validateFields()) {
      return;
    }
    // @ts-ignore
    const { email, password } = getFieldsValues();

    dispatch(
      loginAsync(
        email,
        password,
        () => {
          navigation.replace('Home');
        },
        // @ts-ignore
        (err) => {
          showError(
            err ||
              'Verifique se o usuário e a senha informados estão corretos e tente novamente ',
          );
        },
      ),
    );
  };

  const LoadingIndicator = (props) => (
    <View style={[props.style]}>
      <Spinner size="small" status="control" />
    </View>
  );

  return (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: theme['background-basic-color-1'],
      }}>
      <Body style={{ marginHorizontal: 32 }}>
        <AppLogo />
        {fields}
        <TouchableOpacity
          style={{ marginTop: 16 }}
          onPress={() => {
            navigation.navigate('ForgetPassword');
          }}>
          <Text status="primary" style={{ textAlign: 'center', marginTop: 16 }}>
            Esqueci a senha
          </Text>
        </TouchableOpacity>
        <Button
          accessoryRight={isLoading ? LoadingIndicator : undefined}
          onPress={() => {
            callLogin();
          }}
          status="primary"
          style={{
            marginTop: 16,
            height: 32,
            marginHorizontal: 24,
            ...generateShadow(2),
          }}>
          Entrar
        </Button>
        <View style={{ marginHorizontal: 24 }}>
          <SocialLoginButtons />
        </View>
        <TouchableOpacity
          style={{ marginTop: 16 }}
          onPress={() => {
            navigation.navigate('SignUp');
          }}>
          <Text status="primary" style={{ textAlign: 'center', marginTop: 16 }}>
            Não tem uma conta? Cadastre aqui
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{ marginTop: 16 }}
          onPress={() => {
            navigation.navigate('Home');
          }}>
          <Text
            status="basic"
            style={{
              textAlign: 'center',
              marginTop: 16,
              color: theme['color-basic-600'],
            }}>
            Entrar mais tarde...
          </Text>
        </TouchableOpacity>
      </Body>
    </SafeAreaView>
  );
};
