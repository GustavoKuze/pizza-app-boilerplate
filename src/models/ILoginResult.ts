type userResponse = {
  currentAuthority: string;
  email: string;
  userId: number;
};

export interface ILoginResult {
  token?: string;
  user?: userResponse;
  error?: string;
}
