import React from 'react';
import { Divider as UIKittenDivider } from '@ui-kitten/components';
import { colors } from '@/constants';

type Props = {
  style?: any;
};

export const Divider: React.FC<Props> = ({ style = {} }) => {
  return (
    <UIKittenDivider
      style={{ backgroundColor: colors.divider, marginVertical: 8, ...style }}
    />
  );
};
