export interface ICreditCard {
  id: number;
  number: string;
  name: string;
  cpfCnpj: string;
  expiration: string;
  cvv: string;
  cardBrand?: string;
  userId?: number;
  createdAt?: Date;
  updatedAt?: Date;
}
