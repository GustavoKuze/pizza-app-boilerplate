import React, { useState } from 'react';
import {
  Dimensions,
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp, useFocusEffect } from '@react-navigation/native';
import styles from './styles';
import { TitleBar, TouchableIcon, Button } from '@/components';
import { Layout, Text, useTheme, Divider, Icon } from '@ui-kitten/components';
import { Body, CenteredView } from '@sc/index';
import { FlatList } from 'react-native-gesture-handler';
import { generateShadow } from '@/utils';
import { ICreditCard } from '@/models/ICreditCard';
import { useDispatch, useSelector } from 'react-redux';
import useModal from '@/hooks/useModal';
import { getCreditCardsAsync } from '@/redux/ducks/creditCardDuck';
import { deleteCreditCard } from '@/services/creditCard';
import { setPaymentMethod } from '@/redux/ducks/cartDuck';

type RootStackParamList = {
  CreditCardList: { onSelect?: Function | undefined };
  EditCreditCard: { creditCard: ICreditCard | undefined };
};

type Props = {
  route: RouteProp<RootStackParamList, 'CreditCardList'>;
  navigation: StackNavigationProp<RootStackParamList, 'CreditCardList'>;
};

export const CreditCardList: React.FC<Props> = ({ navigation, route }) => {
  const {
    cart: { payment: cartPayment },
  } = useSelector((state: any) => state.cart);
  const [selectedCardId, setSelectedCardId] = useState(
    (cartPayment.card || {}).id || 0,
  );
  const { userId } = useSelector((state: any) => state.user);
  const { creditCards }: { creditCards: ICreditCard[] } = useSelector(
    (state: any) => state.creditCard,
  );
  const [isLoading, setIsLoading] = useState(true);
  const theme = useTheme();
  const dispatch = useDispatch();
  const { showQuestion } = useModal();

  useFocusEffect(
    React.useCallback(() => {
      setIsLoading(true);
      dispatch(
        getCreditCardsAsync(
          userId,
          () => {
            setIsLoading(false);
          },
          () => {
            setIsLoading(false);
          },
        ),
      );
    }, []),
  );

  const renderBtnCreateCreditCard = () => (
    <Button
      onPress={() => {
        navigation.navigate('EditCreditCard', { creditCard: undefined });
      }}
      status="info"
      style={{ marginHorizontal: 58, marginTop: 8 }}>
      Cadastrar Cartão
    </Button>
  );

  const isCreditCardSelected = (creditCardId) =>
    selectedCardId === creditCardId;

  const callConfirmDelete = async (creditCard: ICreditCard) => {
    showQuestion(
      'Tem certeza de que deseja excluir este cartão?',
      async () => {
        setIsLoading(true);
        await deleteCreditCard(creditCard.id);

        if (isCreditCardSelected(creditCard.id)) {
          dispatch(
            setPaymentMethod(
              // @ts-ignore
              cartPayment?.id
                ? {
                    id: cartPayment.id,
                    name: cartPayment.name,
                    card: undefined,
                  }
                : {},
            ),
          );
          setSelectedCardId(0);
        }

        dispatch(
          getCreditCardsAsync(
            userId,
            () => {
              setIsLoading(false);
            },
            () => {
              setIsLoading(false);
            },
          ),
        );
      },
      () => {},
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <TitleBar
        title={'Meus Cartões'}
        // onGoBack={
        //   typeof route.params?.onSelect !== 'function'
        //     ? () => navigation.goBack()
        //     : undefined
        // }
        onGoBack={() => navigation.goBack()}
      />

      <Divider />

      <Body style={{ paddingBottom: 32 }}>
        {isLoading ? (
          <ActivityIndicator
            style={{ marginTop: Dimensions.get('screen').height / 3 }}
            size={64}
            color={theme['color-primary-500']}
          />
        ) : (
          <>
            {creditCards.length > 0 ? (
              <>
                {typeof route.params?.onSelect === 'function' && (
                  <Text
                    category="s2"
                    style={{ fontSize: 18, marginLeft: 8, marginBottom: 16 }}>
                    Selecione o cartão de crédito
                  </Text>
                )}

                <FlatList
                  contentContainerStyle={{}}
                  data={creditCards}
                  renderItem={({ item }) => (
                    <TouchableOpacity
                      onPress={() =>
                        typeof route.params?.onSelect === 'function'
                          ? setSelectedCardId(item.id)
                          : navigation.navigate('EditCreditCard', {
                              creditCard: item,
                            })
                      }>
                      <Layout
                        style={{
                          marginHorizontal: 8,
                          marginVertical: 8,
                          padding: item.id === selectedCardId ? 13 : 16,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          borderRadius: 12,
                          borderWidth: item.id === selectedCardId ? 3 : 0,
                          borderColor: theme['color-primary-500'],
                          ...generateShadow(6),
                        }}>
                        <Layout>
                          <Text>{`**** **** **** ${
                            item.number.split(' ')[3]
                          }`}</Text>
                          <Text>{item.expiration}</Text>
                          <Text>{item.name}</Text>
                        </Layout>
                        <Layout>
                          <TouchableIcon
                            size={24}
                            onPress={() => {
                              callConfirmDelete(item);
                            }}
                            name="trash-2-outline"
                            color={theme['color-danger-500']}
                            style={{ marginBottom: 32 }}
                          />
                          <TouchableIcon
                            size={24}
                            onPress={() => {
                              navigation.navigate('EditCreditCard', {
                                creditCard: item,
                              });
                            }}
                            name="edit-outline"
                            color={theme['color-warning-500']}
                          />
                        </Layout>
                      </Layout>
                    </TouchableOpacity>
                  )}
                  keyExtractor={(dataItem) => `${dataItem.id}`}
                  refreshing={false}
                  initialNumToRender={40}
                  maxToRenderPerBatch={40}
                />
                <Divider />
                {renderBtnCreateCreditCard()}
                {typeof route.params?.onSelect === 'function' && (
                  <Button
                    disabled={!selectedCardId}
                    onPress={() => {
                      // @ts-ignore
                      route.params?.onSelect(
                        creditCards.find(({ id }) => id === selectedCardId),
                      );
                    }}
                    status="primary"
                    style={{ marginHorizontal: 58, marginTop: 12 }}>
                    Continuar
                  </Button>
                )}
              </>
            ) : (
              <CenteredView>
                <Icon
                  fill={theme['text-hint-color']}
                  style={{
                    width: Dimensions.get('screen').width / 3,
                    height: Dimensions.get('screen').width / 3,
                    marginTop: '20%',
                  }}
                  name="credit-card-outline"
                />
                <Text
                  category="h5"
                  style={{
                    color: theme['text-hint-color'],
                    textAlign: 'center',
                    marginBottom: 8,
                  }}>
                  Você ainda não tem nenhum cartão cadastrado
                </Text>
                {renderBtnCreateCreditCard()}
              </CenteredView>
            )}
          </>
        )}
      </Body>
    </SafeAreaView>
  );
};
