const typesPrefix = '@address';

export const types = {
  SET_LOADING: `${typesPrefix}/SET_LOADING`,
  GET_ADDRESSES_ASYNC: `${typesPrefix}/GET_ADDRESSES_ASYNC`,
  SET_ADDRESSES: `${typesPrefix}/SET_ADDRESSES`,
};

const INITIAL_STATE: {
  addresses: any[];
  isLoading: Boolean;
} = {
  addresses: [],
  isLoading: false,
};

export default (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case types.SET_ADDRESSES:
      return { ...state, addresses: action.payload };
    case types.SET_LOADING:
      return { ...state, isLoading: action.payload };
    default:
      return state;
  }
};

export const setAddresses = (addresses: any) => ({
  type: types.SET_ADDRESSES,
  payload: addresses,
});

export const getAddressesAsync = (
  userId: number,
  onSuccess = () => {},
  onError = () => {},
) => ({
  type: types.GET_ADDRESSES_ASYNC,
  payload: {
    userId,
    onSuccess,
    onError,
  },
});

export const setIsLoading = (isLoading: Boolean) => ({
  type: types.SET_LOADING,
  payload: isLoading,
});

export const allActions = {
  getAddressesAsync,
  setAddresses,
  setIsLoading,
};
