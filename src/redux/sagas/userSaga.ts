import { put, call } from 'redux-saga/effects';
import { login, signUp } from '@/services/auth';
import { types as userTypes } from '@/redux/ducks/userDuck';

function* postLogin(action: any) {
  yield put({
    type: userTypes.SET_LOADING,
    payload: true,
  });

  const { email, password, onSuccess, onError } = action.payload;

  const result = yield call(() => login(email, password));

  if (!result.error) {
    const {
      token,
      user: { currentAuthority, userId },
    } = result;
    yield put({
      type: userTypes.SET_LOGIN_DATA,
      payload: { token, email, currentAuthority, userId },
    });
    if (typeof onSuccess === 'function') {
      onSuccess();
    }
  } else {
    if (typeof onError === 'function') {
      onError(result.error);
    }
  }

  yield put({
    type: userTypes.SET_LOADING,
    payload: false,
  });
}

function* postSignup(action: any) {
  yield put({
    type: userTypes.SET_LOADING,
    payload: true,
  });

  const { email, password, name, onSuccess, onError } = action.payload;

  const result = yield call(() => signUp(email, password, name));

  if (!result.error) {
    yield put({
      type: userTypes.LOGIN_ASYNC,
      payload: { email, password },
    });
    if (typeof onSuccess === 'function') {
      onSuccess();
    }
  } else {
    if (typeof onError === 'function') {
      onError(result.error);
    }
  }

  yield put({
    type: userTypes.SET_LOADING,
    payload: false,
  });
}

export { postLogin, postSignup };
