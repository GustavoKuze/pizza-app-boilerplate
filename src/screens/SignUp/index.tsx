import React, { useState } from 'react';
import { SafeAreaView, TouchableOpacity, View } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles from './styles';
import { CheckBox, Text, useTheme } from '@ui-kitten/components';
import { Body } from '@sc/index';
import { Button, AppLogo, SocialLoginButtons } from '@/components';
import { useFields } from '@/hooks/useFields';
import { useDispatch } from 'react-redux';
import { signupAsync } from '@/redux/ducks/userDuck';
import useModal from '@/hooks/useModal';
import { generateShadow } from '@/utils';

type RootStackParamList = {
  SignUp: undefined;
  Login: undefined;
  Home: undefined;
};

type Props = {
  route: RouteProp<RootStackParamList, 'SignUp'>;
  navigation: StackNavigationProp<RootStackParamList, 'SignUp'>;
};

export const SignUp: React.FC<Props> = ({ navigation }) => {
  const [checked, setChecked] = useState<boolean | undefined>(false);
  const dispatch = useDispatch();
  const { showError } = useModal();
  const theme = useTheme();

  const [fields, validateFields, getFieldsValues] = useFields(
    [
      {
        name: 'name',
        label: 'Nome',
        placeholder: 'Informe seu nome completo',
        next: 'email',
        required: true,
      },
      {
        name: 'email',
        label: 'E-mail',
        keyboardType: 'email-address',
        placeholder: 'Informe seu melhor e-mail',
        next: 'password',
        regex: /\S+@\S+\.\S+/,
        required: true,
      },
      {
        name: 'password',
        label: 'Senha',
        placeholder: 'Digite uma senha forte',
        required: true,
        secure: true,
      },
    ],
    // @ts-ignore
    () => {
      callSignUp();
    },
  );

  const callSignUp = () => {
    // @ts-ignore
    if (!validateFields() || !checked) {
      return;
    }

    // @ts-ignore
    const { email, password, name } = getFieldsValues();

    dispatch(
      signupAsync(
        email,
        password,
        name,
        () => {
          navigation.popToTop();
        },
        // @ts-ignore
        (err) => {
          showError(
            err ||
              'Verifique se o usuário e a senha informados estão corretos e tente novamente ',
          );
        },
      ),
    );
  };

  return (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: theme['background-basic-color-1'],
      }}>
      <Body style={{ marginHorizontal: 32 }}>
        <AppLogo />
        {fields}
        <CheckBox
          style={{ marginTop: 16 }}
          checked={checked}
          onChange={(nextChecked) => setChecked(nextChecked)}>
          Declaro que li e aceito os seguintes
        </CheckBox>
        <TouchableOpacity onPress={() => {}}>
          <Text status="info" style={{ marginLeft: 32 }} category="label">
            Termos de Uso e Política de Privacidade
          </Text>
        </TouchableOpacity>
        <Button
          onPress={() => {
            callSignUp();
          }}
          status="primary"
          style={{
            marginTop: 16,
            height: 32,
            marginHorizontal: 32,
            ...generateShadow(6),
          }}>
          Cadastrar
        </Button>
        <View style={{ marginHorizontal: 32 }}>
          <SocialLoginButtons />
        </View>
        <TouchableOpacity
          style={{ marginTop: 16 }}
          onPress={() => {
            navigation.navigate('Login');
          }}>
          <Text status="primary" style={{ textAlign: 'center', marginTop: 16 }}>
            Já tem uma conta? Entre aqui
          </Text>
        </TouchableOpacity>
      </Body>
    </SafeAreaView>
  );
};
