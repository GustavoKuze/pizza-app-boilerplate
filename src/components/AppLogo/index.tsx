import { Avatar, Text } from '@ui-kitten/components';
import React from 'react';
import CenteredView from '@sc/CenteredView';
import { appDisplayName } from '@/config/settings.json';

export const AppLogo: React.FC = () => {
  return (
    <CenteredView>
      <Avatar
        source={require('@/assets/logo-transparent.png')}
        size="giant"
        style={{
          marginTop: 32,
          height: 104,
          width: 104,
        }}
        shape="round"
      />
      <Text category="h6" style={{ fontWeight: 'bold' }}>
        {appDisplayName}
      </Text>
    </CenteredView>
  );
};
