import { Text } from '@ui-kitten/components';
import styled from 'styled-components/native';

export const PriceStyled = styled(Text).attrs({
  category: 'h5',
  status: 'warning',
})`
  margin-top: 8px;
  ${({ color }) => (color ? `color: ${color};` : undefined)};
  font-size: ${({ fontSize }) => fontSize || 22};
  font-weight: ${({ fontWeight }) => fontWeight || 'bold'};
`;

export default PriceStyled;
