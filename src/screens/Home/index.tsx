/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useState } from 'react';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles from './styles';
import { useTheme, Icon, Text } from '@ui-kitten/components';
import { TitleBar, Carousel, Divider, Button } from '@/components';
import { CenteredView } from '@sc/index';
import { SafeAreaView } from 'react-native-safe-area-context';
import { useDispatch, useSelector } from 'react-redux';
import { appDisplayName } from '@/config/settings.json';
import { ScrollView, ActivityIndicator, Dimensions } from 'react-native';
import MenuSection from './MenuSection';
import { getMenuAsync } from '@/redux/ducks/menuDuck';
import { getBanners } from '@/services/banner';

type RootStackParamList = {
  Home: undefined;
  AccountSettings: undefined;
};

type Props = {
  route: RouteProp<RootStackParamList, 'Home'>;
  navigation: StackNavigationProp<RootStackParamList, 'Home'>;
};

export const Home: React.FC<Props> = ({ navigation }) => {
  const { categories = [], isLoading } = useSelector(
    (state: any) => state.menu,
  );
  const theme = useTheme();
  const dispatch = useDispatch();
  const [isLoadingBanners, setIsLoadingBanners] = useState(true);
  const [carouselItems, setCarouselItems] = useState<any[]>([]);

  const loadHome = async () => {
    dispatch(getMenuAsync());
    setIsLoadingBanners(true);
    const banners = await getBanners();
    setCarouselItems(banners);
    setIsLoadingBanners(false);
  };

  useEffect(() => {
    loadHome();
  }, []);

  return (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: theme['background-basic-color-1'],
      }}>
      <TitleBar title={appDisplayName} />
      <Divider />
      <ScrollView>
        {isLoading || isLoadingBanners ? (
          <ActivityIndicator
            style={{ marginTop: Dimensions.get('screen').height / 3 }}
            size={64}
            color={theme['color-primary-500']}
          />
        ) : (
          <>
            <Carousel
              isLoading={isLoadingBanners}
              items={carouselItems}
              navigation={navigation}
            />
            {categories.length > 0 ? (
              categories.map(({ name, products, id }) => (
                <MenuSection
                  navigation={navigation}
                  key={`section-${id}`}
                  name={name}
                  products={products}
                />
              ))
            ) : (
              <CenteredView
                style={{
                  marginHorizontal: 24,
                  marginTop: Dimensions.get('screen').height / 4,
                }}>
                <Icon
                  fill={theme['color-danger-transparent-600']}
                  style={{
                    width: Dimensions.get('screen').width / 5,
                    height: Dimensions.get('screen').width / 5,
                  }}
                  name="wifi-off-outline"
                />
                <Text
                  category="h5"
                  style={{
                    color: theme['color-danger-transparent-600'],
                    textAlign: 'center',
                    marginBottom: 8,
                  }}>
                  {'Falha na conexão com o servidor'}
                </Text>
                <Button
                  onPress={loadHome}
                  status="primary"
                  style={{ marginHorizontal: 58, marginTop: 8 }}>
                  Tentar Novamente
                </Button>
              </CenteredView>
            )}
          </>
        )}
      </ScrollView>
    </SafeAreaView>
  );
};
