import React from 'react';
import { useDispatch } from 'react-redux';
import { setIsModalOpen, setModalContent } from '@/redux/ducks/modalDuck';
import { Text } from '@ui-kitten/components';
import { Button } from '@/components';

const useModal = () => {
  const dispatch = useDispatch();

  const showError = (message: string, buttonLabel = 'OK') => {
    dispatch(
      setModalContent({
        message,
        isOpen: true,
        status: 'danger',
        buttons: [
          <Button
            status="danger"
            onPress={() => dispatch(setIsModalOpen(false))}>
            {buttonLabel}
          </Button>,
        ],
      }),
    );
  };

  const showQuestion = (message: any, onOk = () => {}, onCancel = () => {}) => {
    dispatch(
      setModalContent({
        message: <Text category="p1">{message}</Text>,
        isOpen: true,
        buttons: [
          <Button
            onPress={() => {
              onCancel();
              dispatch(setIsModalOpen(false));
            }}
            style={{ width: '40%', borderRadius: 5 }}
            key="btn-no"
            status="danger">
            Não
          </Button>,
          <Button
            onPress={() => {
              onOk();
              dispatch(setIsModalOpen(false));
            }}
            style={{ width: '40%', borderRadius: 5 }}
            key="btn-yes"
            status="success">
            Sim
          </Button>,
        ],
      }),
    );
  };

  const closeModal = () => {
    dispatch(setIsModalOpen(false));
  };

  return {
    showError,
    showQuestion,
    closeModal,
  };
};

export default useModal;
