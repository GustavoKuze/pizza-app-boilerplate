import { toCurrency } from '@/utils';
import React from 'react';

import { PriceStyled } from './styles';

type Props = {
  value: number;
  style?: any;
};

export const Price: React.FC<Props> = ({ value, style = {} }) => {
  return <PriceStyled style={style}>{toCurrency(value)}</PriceStyled>;
};
