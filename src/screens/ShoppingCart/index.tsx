import React, { useState } from 'react';
import { SafeAreaView, ScrollView } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles from './styles';
import {
  Divider,
  useTheme,
  Text,
  Icon,
  Layout,
  Spinner,
} from '@ui-kitten/components';
import { Button, Price, TitleBar } from '@/components';
import CartItem from './CartItem';
import { Body, CenteredView } from '@/components/StyledComponents';
import { useFields } from '@/hooks/useFields';
import { generateShadow, getProductFullPrice } from '@/utils';
import EmptyCart from './EmptyCart';
import { useDispatch, useSelector } from 'react-redux';
import {
  addCoupon,
  setAddress,
  setDeliveryFee,
  setPaymentMethod,
} from '@/redux/ducks/cartDuck';
import { isCouponValid, listDeliveryFees } from '@/services/order';
import { IAddress } from '@/models/IAddress';
import { View } from 'react-native';

type RootStackParamList = {
  ShoppingCart: undefined;
  ProductDetails: { productId: number };
  Home: undefined;
  OrderPreview: undefined;
  PaymentMethods: { onSelect?: Function | undefined };
  AddressList: { onSelect?: Function | undefined };
  CreditCardList: { onSelect?: Function | undefined };
};

type Props = {
  route: RouteProp<RootStackParamList, 'ShoppingCart'>;
  navigation: StackNavigationProp<RootStackParamList, 'ShoppingCart'>;
};

export const ShoppingCart: React.FC<Props> = ({ route, navigation }) => {
  const {
    cart: { address, payment, coupons, deliveryFee, items },
    isLoading,
  } = useSelector((state: any) => state.cart);

  const dispatch = useDispatch();
  const theme = useTheme();
  const [isValidCoupon, setIsValidCoupon] = useState<boolean | undefined>();
  const [isLoadingCoupon, setIsLoadingCoupon] = useState<boolean>(false);
  const [fields, validateFields, getFieldsValues] = useFields(
    [
      {
        name: 'coupon',
        label: 'Cupom de desconto',
        placeholder: 'Digite o seu cupom',
        required: true,
      },
    ],
    // @ts-ignore
    () => {
      applyCouponIfValid();
    },
  );

  const setOrderDeliveryFeedByAddress = async (address: IAddress) => {
    const deliveryFees = await listDeliveryFees();
    const deliveryFee =
      deliveryFees.find(
        ({ neighborhood }) =>
          neighborhood.toLowerCase() === address.neighborhood.toLowerCase(),
      ) || deliveryFees.find(({ neighborhood }) => neighborhood === 'default');

    dispatch(setDeliveryFee(deliveryFee));

    return deliveryFees;
  };

  const startBuyingFlow = () => {
    navigation.navigate('AddressList', {
      onSelect: async (address) => {
        await setOrderDeliveryFeedByAddress(address);
        dispatch(setAddress(address));
        navigation.navigate('PaymentMethods', {
          onSelect: (paymentMethod, currentCard) => {
            const { id, name } = paymentMethod;

            dispatch(
              setPaymentMethod({
                id,
                name,
                card:
                  name === 'debit' || name === 'credit'
                    ? currentCard
                    : undefined,
              }),
            );

            if (name === 'credit' || name === 'debit') {
              navigation.navigate('CreditCardList', {
                onSelect: (card) => {
                  dispatch(
                    setPaymentMethod({
                      id,
                      name,
                      card,
                    }),
                  );
                  navigation.navigate('OrderPreview');
                },
              });
            } else {
              navigation.navigate('OrderPreview');
            }
          },
        });
      },
    });
  };

  const getItemsSum = () => {
    return items
      .map(
        ({ price, quantity, selectedVariationsOptions }) =>
          getProductFullPrice({ price }, selectedVariationsOptions) * quantity,
      )
      .reduce((a, b) => a + b);
  };

  const isCouponAlreadyApplied = (code = '') => {
    return !!coupons.find(
      (coupon) => code.toUpperCase() === coupon.code.toUpperCase(),
    );
  };

  const applyCouponIfValid = async () => {
    // @ts-ignore
    if (!validateFields()) {
      return false;
    }

    // @ts-ignore
    const { coupon } = getFieldsValues();

    if (isCouponAlreadyApplied(coupon)) {
      return false;
    }

    setIsLoadingCoupon(true);
    const validCoupon = await isCouponValid(coupon);

    if (validCoupon) {
      const { id, code, discountPercentage } = validCoupon;
      dispatch(
        addCoupon({
          id,
          code,
          discountPercentage,
        }),
      );
    }

    setIsValidCoupon(!!validCoupon);
    setIsLoadingCoupon(false);
  };

  const LoadingIndicator = (props) => (
    <View style={[props.style]}>
      <Spinner size="small" status="control" />
    </View>
  );

  return (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: theme['background-basic-color-1'],
      }}>
      <TitleBar title={'Carrinho'} />
      <Divider />

      {!items.length ? (
        <EmptyCart navigation={navigation} />
      ) : (
        <>
          <ScrollView>
            <Body>
              {items.map((item) => (
                <CartItem
                  key={item.id}
                  onPress={() => {
                    navigation.navigate('ProductDetails', {
                      productId: item.id,
                    });
                  }}
                  product={item}
                />
              ))}

              <Divider style={{ marginTop: 16 }} />

              <CenteredView row style={{ marginTop: 16 }}>
                {fields}
                <Button
                  accessoryRight={
                    isLoadingCoupon ? LoadingIndicator : undefined
                  }
                  onPress={() => {
                    applyCouponIfValid();
                  }}
                  status="success"
                  style={{
                    marginTop: 16,
                    height: 32,
                    marginLeft: 6,
                    paddingVertical: 0,
                    ...generateShadow(2),
                  }}>
                  Aplicar cupom
                </Button>
              </CenteredView>

              {typeof isValidCoupon !== 'undefined' ? (
                <CenteredView row>
                  <Icon
                    name={isValidCoupon ? 'pricetags-outline' : 'close-outline'}
                    fill={
                      theme[
                        isValidCoupon ? 'color-success-500' : 'color-danger-500'
                      ]
                    }
                    style={{ width: 24, height: 24, marginRight: 4 }}
                  />
                  <Text status={isValidCoupon ? 'success' : 'danger'}>
                    {isValidCoupon ? 'Desconto aplicado' : 'Cupom inválido'}
                  </Text>
                </CenteredView>
              ) : undefined}

              <Divider style={{ marginTop: 16 }} />

              <Layout style={{ alignItems: 'flex-end', marginTop: 32 }}>
                <Text category="h6" style={{ fontWeight: 'bold' }}>
                  SUBTOTAL
                </Text>
                <Price
                  style={{ color: theme['text-hint-color'], fontSize: 18 }}
                  value={getItemsSum()}
                />
                {/* <Price
                  style={{ color: theme['text-hint-color'], fontSize: 18 }}
                  value={-3}
                /> */}
                <Text
                  category="h6"
                  style={{ fontWeight: 'bold', marginTop: 8 }}>
                  TOTAL
                </Text>
                <Price value={getItemsSum()} />
              </Layout>
              <Button
                onPress={() => startBuyingFlow()}
                status="primary"
                style={{
                  marginVertical: 58,
                  ...generateShadow(2),
                }}>
                Continuar
              </Button>
            </Body>
          </ScrollView>
        </>
      )}
    </SafeAreaView>
  );
};
