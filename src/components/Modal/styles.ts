import { StyleSheet, Dimensions } from 'react-native';

const styles = StyleSheet.create({
  container: {},
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  btnOk: {},
  modalBody: {
    width: Dimensions.get('window').width - 24,
  },
  modalTitle: {
    marginBottom: 24,
  },
  modalMessage: {
    marginBottom: 18,
  },
  closeIcon: {
    height: 32,
    width: 32,
  },
  button: {
    height: 38,
    width: 38,
    backgroundColor: '#f5f5f5',
    borderRadius: 100,
    marginTop: 8,
    marginBottom: 8,
    marginLeft: 'auto',
  },
});

export default styles;
