import React, { useState } from 'react';
import { SafeAreaView, ScrollView, View } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles from './styles';
import {
  Divider,
  useTheme,
  Text,
  Layout,
  Spinner,
} from '@ui-kitten/components';
import { Price, TitleBar, Button, Product } from '@/components';
import { Body } from '@/components/StyledComponents';
import { generateShadow, getProductFullPrice } from '@/utils';
import { useSelector } from 'react-redux';
import { paymentMethodsFriendlyLabels } from '@/constants';
import { createOrder } from '@/services/order';

type RootStackParamList = {
  OrderPreview: undefined;
  OrderDetails: { orderId: number };
  ProductDetails: { productId: number };
};

type Props = {
  route: RouteProp<RootStackParamList, 'OrderPreview'>;
  navigation: StackNavigationProp<RootStackParamList, 'OrderPreview'>;
};

export const OrderPreview: React.FC<Props> = ({ route, navigation }) => {
  const theme = useTheme();
  const {
    cart: { address, payment, coupons, deliveryFee, status, items },
    isLoading,
  } = useSelector((state: any) => state.cart);
  const [isLoadingCreateOrder, setIsLoadingCreateOrder] = useState(false);

  const { userId } = useSelector((state: any) => state.user);

  const callCheckout = async () => {
    setIsLoadingCreateOrder(true);
    const checkoutResult = await createOrder({
      address,
      payment,
      user: {
        id: userId,
        name: '',
        email: '',
        username: '',
        phone: '',
      },
      coupons,
      deliveryFee,
      status,
      items: items.map(({ id, ...item }) => ({ productId: id, ...item })),
    });

    console.log(checkoutResult);

    navigation.navigate('OrderDetails', {
      orderId: checkoutResult.data.data.id,
    });

    setIsLoadingCreateOrder(false);
  };

  const getItemsSum = () =>
    items
      .map(
        ({ price, quantity, selectedVariationsOptions }) =>
          getProductFullPrice({ price }, selectedVariationsOptions) * quantity,
      )
      .reduce((a, b) => a + b);

  const getTotalPrice = () => {
    let totalPrice = getItemsSum();
    const discountPercentagesSum =
      coupons.length > 0
        ? coupons.map((c) => c.discountPercentage).reduce((a, b) => a + b)
        : 0;

    totalPrice -= totalPrice * (discountPercentagesSum / 100);

    totalPrice += deliveryFee.value;
    return totalPrice;
  };

  const LoadingIndicator = (props) => (
    <View style={[props.style]}>
      <Spinner size="small" status="control" />
    </View>
  );

  return (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: theme['background-basic-color-1'],
      }}>
      <TitleBar
        title={'Revisão do pedido'}
        onGoBack={() => navigation.goBack()}
      />
      <Divider />

      <ScrollView>
        <Body style={{ paddingBottom: 8 }}>
          {items.map((item) => (
            <Product
              key={item.id}
              onPress={() => {
                navigation.navigate('ProductDetails', {
                  productId: item.id,
                });
              }}
              product={item}
            />
          ))}

          <Divider style={{ marginVertical: 16 }} />

          {coupons.length > 0 ? (
            <>
              <Text category="p1" style={{ fontWeight: 'bold' }}>
                Cupons Aplicados:
              </Text>
              {coupons.map((coupon) => (
                <Text category="label" style={{ fontSize: 14 }}>
                  {`- #${coupon.code} (${coupon.discountPercentage}%)`}
                </Text>
              ))}

              <Divider style={{ marginVertical: 16 }} />
            </>
          ) : undefined}

          <Text category="p1" style={{ fontWeight: 'bold' }}>
            Endereço:
          </Text>
          <Text category="label" style={{ fontSize: 14 }}>
            {address.name}
          </Text>
          <Text category="label" style={{ fontSize: 14 }}>
            {address.street}
          </Text>
          <Text category="label" style={{ fontSize: 14 }}>
            Nº {address.number}
          </Text>
          <Text category="label" style={{ fontSize: 14 }}>
            {address.neighborhood}
          </Text>
          <Text category="label" style={{ fontSize: 14 }}>
            {address.city}
          </Text>

          <Divider style={{ marginVertical: 16 }} />

          <Text category="p1" style={{ fontWeight: 'bold' }}>
            Forma de pagamento:
          </Text>
          <Text category="label" style={{ fontSize: 14 }}>
            {paymentMethodsFriendlyLabels[payment.name]}
          </Text>
          {payment.card ? (
            <>
              <Text category="label" style={{ fontSize: 14 }}>
                {`**** **** **** ${payment.card.number.split(' ')[3]}`}
              </Text>
              <Text category="label" style={{ fontSize: 14 }}>
                {payment.card.name}
              </Text>
              <Text category="label" style={{ fontSize: 14 }}>
                {payment.card.expiration}
              </Text>
            </>
          ) : undefined}

          <Divider style={{ marginTop: 16 }} />

          <Layout style={{ alignItems: 'flex-end', marginTop: 32 }}>
            <Text category="h6" style={{ fontWeight: 'bold' }}>
              SUBTOTAL
            </Text>

            <Text
              category="label"
              style={{
                color: theme['text-hint-color'],
              }}>
              Itens:
            </Text>
            <Price
              style={{
                color: theme['text-hint-color'],
                fontSize: 18,
                marginTop: 0,
              }}
              value={getItemsSum()}
            />

            {coupons.length > 0 ? (
              <>
                <Text
                  category="label"
                  style={{ color: theme['text-hint-color'], marginTop: 2 }}>
                  Cupons:
                </Text>

                {coupons.map((coupon) => (
                  <Price
                    style={{
                      color: theme['text-hint-color'],
                      fontSize: 18,
                      marginTop: 0,
                    }}
                    value={-(getItemsSum() * (coupon.discountPercentage / 100))}
                  />
                ))}
              </>
            ) : undefined}

            <Text
              category="label"
              style={{ color: theme['text-hint-color'], marginTop: 2 }}>
              Entrega:
            </Text>
            <Price
              style={{
                color: theme['text-hint-color'],
                fontSize: 18,
                marginTop: 0,
              }}
              value={deliveryFee.value}
            />

            <Text category="h6" style={{ fontWeight: 'bold', marginTop: 8 }}>
              TOTAL
            </Text>
            <Price
              style={{
                marginTop: 0,
              }}
              value={getTotalPrice()}
            />
          </Layout>
          <Button
            accessoryRight={isLoadingCreateOrder ? LoadingIndicator : undefined}
            onPress={() => callCheckout()}
            status="primary"
            style={{
              marginTop: 32,
              marginBottom: 58,
              ...generateShadow(2),
            }}>
            Finalizar Pedido
          </Button>
        </Body>
      </ScrollView>
    </SafeAreaView>
  );
};
