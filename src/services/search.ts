import { ISearchProducts } from '@/models/ISearchProductsResults';
import { request } from './api';

const searchProducts = async (name: string): Promise<ISearchProducts[]> => {
  try {
    const response = await request(`/search/products?name=${name}`);

    return response.data.data;
  } catch (err) {
    console.log(err);
    return [];
  }
};

export { searchProducts };
