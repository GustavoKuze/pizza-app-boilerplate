import { ISearchProducts } from '@/models/ISearchProductsResults';

const typesPrefix = '@search';

export const types = {
  SET_SEARCH_RESULT: `${typesPrefix}/SET_SEARCH_RESULT`,
  SEARCH_PRODUCT_ASYNC: `${typesPrefix}/SEARCH_PRODUCT_ASYNC`,
  SET_LOADING: `${typesPrefix}/SET_LOADING`,
};

const INITIAL_STATE: {
  products: ISearchProducts[];
  isLoading: Boolean;
} = {
  products: [],
  isLoading: false,
};

export default (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case types.SET_SEARCH_RESULT:
      return { ...state, products: action.payload };
    case types.SET_LOADING:
      return { ...state, isLoading: action.payload };
    default:
      return state;
  }
};

export const searchProductAsync = (query: string) => ({
  type: types.SEARCH_PRODUCT_ASYNC,
  payload: query,
});

export const setSearchResult = (products: ISearchProducts[]) => ({
  type: types.SET_SEARCH_RESULT,
  payload: products,
});

export const setIsLoadingSearch = (isLoading: Boolean) => ({
  type: types.SET_LOADING,
  payload: isLoading,
});

export const allActions = {
  searchProductAsync,
  setSearchResult,
  setIsLoadingSearch,
};
