import { StyleSheet } from 'react-native';
import styled from 'styled-components/native';
import { Divider } from '@ui-kitten/components';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default styles;

export const SettingsDivider = styled(Divider)`
  margin-vertical: 0px;
`;
