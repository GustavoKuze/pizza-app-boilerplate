export interface IAPIResponse {
  data: any;
  status: number;
  error?: string;
  ok: boolean;
}
