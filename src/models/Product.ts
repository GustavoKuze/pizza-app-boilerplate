export type VariationOption = {
  key: string;
  value: string;
};

export type ProductVariation = {
  name: string;
  options: VariationOption[];
};

export interface Product {
  id: number;
  image: string;
  name: string;
  description: string;
  price: number;
  variations?: ProductVariation[];
}
