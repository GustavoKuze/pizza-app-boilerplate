import { saveState, deleteState } from '@/utils/asyncStorage';

const typesPrefix = '@user';

export const types = {
  SET_LOGIN_DATA: `${typesPrefix}/SET_LOGIN_DATA`,
  SET_LOADING: `${typesPrefix}/SET_LOADING`,
  LOGIN_ASYNC: `${typesPrefix}/LOGIN_ASYNC`,
  SIGNUP_ASYNC: `${typesPrefix}/SIGNUP_ASYNC`,
  LOGOUT: `${typesPrefix}/LOGOUT`,
};

const INITIAL_STATE: {
  token?: string;
  currentAuthority: string;
  email: string;
  userId: number;
  isLoading: Boolean;
} = {
  currentAuthority: 'guest',
  email: '',
  userId: 0,
  token: '',
  isLoading: false,
};

export default (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case types.SET_LOGIN_DATA:
      const newState = { ...state, ...action.payload };
      saveState(newState, 'login');
      return newState;
    case types.LOGOUT:
      deleteState('login');
      return INITIAL_STATE;
    case types.SET_LOADING:
      return { ...state, isLoading: action.payload };
    default:
      return state;
  }
};

type LoginData = {
  token?: string;
  currentAuthority: string;
  email: string;
  userId: number | string;
};

export const setLoginData = (loginData: LoginData | any) => ({
  type: types.SET_LOGIN_DATA,
  payload: loginData,
});

export const loginAsync = (
  email: string,
  password: string,
  onSuccess = () => {},
  onError = () => {},
) => ({
  type: types.LOGIN_ASYNC,
  payload: {
    email,
    password,
    onSuccess,
    onError,
  },
});

export const signupAsync = (
  email: string,
  password: string,
  name: string,
  onSuccess = () => {},
  onError = () => {},
) => ({
  type: types.SIGNUP_ASYNC,
  payload: {
    email,
    password,
    name,
    onSuccess,
    onError,
  },
});

export const setIsLoading = (isLoading: Boolean) => ({
  type: types.SET_LOADING,
  payload: isLoading,
});

export const logOut = () => ({
  type: types.LOGOUT,
});

export const allActions = {
  setLoginData,
  loginAsync,
  signupAsync,
  setIsLoading,
  logOut,
};
