import React, { useState } from 'react';
import {
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp, useFocusEffect } from '@react-navigation/native';
import styles from './styles';
import { TitleBar } from '@/components';
import { Layout, Text, useTheme } from '@ui-kitten/components';
import { Body, SpaceBetweenView } from '@sc/index';
import { FlatList } from 'react-native-gesture-handler';
import { generateShadow } from '@/utils';
import { Order } from '@/models/Order';
import { useDispatch, useSelector } from 'react-redux';
import { getOrdersAsync } from '@/redux/ducks/orderDuck';

type RootStackParamList = {
  OrderList: undefined;
  OrderDetails: { orderId: number };
};

type Props = {
  route: RouteProp<RootStackParamList, 'OrderList'>;
  navigation: StackNavigationProp<RootStackParamList, 'OrderList'>;
};

export const OrderList: React.FC<Props> = ({ navigation }) => {
  const theme = useTheme();
  const { userId } = useSelector((state: any) => state.user);
  const [isLoading, setIsLoading] = useState(true);
  const dispatch = useDispatch();
  const { orders }: { orders: any[] } = useSelector(
    (state: any) => state.order,
  );
  const navigateToDetails = (order: Order) => {
    navigation.navigate('OrderDetails', { orderId: order.id });
  };

  useFocusEffect(
    React.useCallback(() => {
      setIsLoading(true);
      dispatch(
        getOrdersAsync(
          userId,
          () => {
            setIsLoading(false);
          },
          () => {
            setIsLoading(false);
          },
        ),
      );
    }, []),
  );

  return (
    <SafeAreaView style={styles.container}>
      <TitleBar title={'Meus Pedidos'} onGoBack={() => navigation.goBack()} />
      <Body style={{ paddingBottom: 32 }}>
        {isLoading ? (
          <ActivityIndicator
            style={{ marginTop: Dimensions.get('screen').height / 3 }}
            size={64}
            color={theme['color-primary-500']}
          />
        ) : (
          <>
            {orders.length > 0 ? (
              <FlatList
                contentContainerStyle={{}}
                data={orders}
                renderItem={({ item }) => (
                  <TouchableOpacity onPress={() => navigateToDetails(item)}>
                    <Layout
                      style={{
                        marginHorizontal: 8,
                        marginVertical: 8,
                        padding: item.status === 'em_andamento' ? 13 : 16,

                        borderRadius: 12,
                        borderWidth: item.status === 'em_andamento' ? 3 : 0,
                        borderColor: theme['color-primary-500'],
                        ...generateShadow(6),
                      }}>
                      <Layout>
                        <SpaceBetweenView row>
                          <Text category="s1" style={{ fontWeight: 'bold' }}>
                            Pedido: #{item.id}
                          </Text>
                          <Text category="label" style={{ marginLeft: 'auto' }}>
                            {item.date}
                          </Text>
                        </SpaceBetweenView>
                        <Text style={{ marginBottom: 16 }}>{item.status}</Text>

                        {item.items.map((product) => (
                          <Layout
                            key={product.id}
                            style={{
                              alignItems: 'center',
                              flexDirection: 'row',
                            }}>
                            <Text category="s1" style={{ fontWeight: 'bold' }}>
                              {`${product.quantity}x`}
                            </Text>
                            <Text style={{ marginLeft: 8 }}>
                              {product.name}
                            </Text>
                          </Layout>
                        ))}
                      </Layout>
                    </Layout>
                  </TouchableOpacity>
                )}
                keyExtractor={(dataItem) => `${dataItem.id}`}
                refreshing={false}
                initialNumToRender={40}
                maxToRenderPerBatch={40}
              />
            ) : undefined}
          </>
        )}
      </Body>
    </SafeAreaView>
  );
};
