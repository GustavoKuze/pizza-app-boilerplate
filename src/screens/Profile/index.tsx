import React from 'react';
import { SafeAreaView, ToastAndroid } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles, { SettingsDivider } from './styles';
import { TitleBar } from '@/components';
import { List, ListItem } from '@ui-kitten/components';
import { createEvaIcon } from '@/utils/layout';
import { useDispatch, useSelector } from 'react-redux';
import { logOut } from '@/redux/ducks/userDuck';
import auth from '@react-native-firebase/auth';

type RootStackParamList = {
  AccountSettings: undefined;
  Settings: undefined;
};

type Props = {
  route: RouteProp<RootStackParamList, 'AccountSettings'>;
  navigation: StackNavigationProp<RootStackParamList, 'AccountSettings'>;
};

export const Profile: React.FC<Props> = ({ navigation }) => {
  const dispatch = useDispatch();
  const { currentAuthority, email } = useSelector((state: any) => state.user);

  const buildMenuItems = ({ navigation }) => {
    const loggedInItems = [
      {
        icon: 'file-text-outline',
        onPress: () => {
          navigation.navigate('OrderList');
        },
        label: 'Pedidos',
      },
      {
        icon: 'credit-card-outline',
        onPress: () => {
          // navigation.navigate('CreditCardList', { onSelect: (card) => {} });
          navigation.navigate('CreditCardList');
        },
        label: 'Cartões',
      },
      {
        icon: 'pin-outline',
        onPress: () => {
          // navigation.navigate('AddressList', { onSelect: (address) => {} });
          navigation.navigate('AddressList');
        },
        label: 'Endereços',
      },
    ];

    const guestItems = [
      {
        icon: 'shield-outline',
        onPress: () => {
          navigation.navigate('Help');
        },
        label: 'Privacidade',
      },
      {
        icon: 'question-mark-circle-outline',
        onPress: () => {
          navigation.navigate('Help');
        },
        label: 'Ajuda',
      },
      {
        icon: 'info-outline',
        onPress: () => {
          navigation.navigate('About');
        },
        label: 'Sobre',
      },
      {
        icon: 'share-outline',
        onPress: () => {
          ToastAndroid.show('Convidar um amigo', ToastAndroid.SHORT);
        },
        label: 'Compartilhar',
      },
      {
        icon: 'log-out-outline',
        onPress: () => {
          dispatch(logOut());
          auth().signOut();
          // if (currentAuthority !== 'guest') {
          //   navigation.popToTop();
          // }
          navigation.replace('Login');
        },
        label: currentAuthority === 'guest' ? 'Fazer Login' : 'Sair',
      },
    ];

    return [
      ...(currentAuthority !== 'guest' ? loggedInItems : []),
      ...guestItems,
    ];
  };
  return (
    <SafeAreaView style={styles.container}>
      <TitleBar title={currentAuthority === 'guest' ? 'Bem-vindo(a)' : email} />
      <SettingsDivider />
      <List
        data={buildMenuItems({ navigation })}
        ItemSeparatorComponent={SettingsDivider}
        renderItem={({ item: { icon, label, onPress } }) => (
          <ListItem
            accessoryLeft={createEvaIcon(icon)}
            title={`${label}`}
            onPress={onPress}
          />
        )}
      />
    </SafeAreaView>
  );
};
