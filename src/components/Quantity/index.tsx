import React from 'react';
import { TextInput, View } from 'react-native';
import { useTheme } from '@ui-kitten/components';
import { isDarkTheme } from '@/utils';
import { QuantityIcon, QtyMinusButton, QtyPlusButton } from './styles';

// import { Container } from './styles';

type Props = {
  value: number;
  setValue(value: number);
  style?: any;
};

export const Quantity: React.FC<Props> = ({ value, setValue, style = {} }) => {
  const theme = useTheme();

  return (
    <View
      style={{
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        ...style,
      }}>
      <QtyMinusButton
        backgroundColor={theme['border-basic-color-3']}
        onPress={() => setValue(Number(value) - 1)}>
        <QuantityIcon name="minus-outline" fill={theme['text-hint-color']} />
      </QtyMinusButton>
      <TextInput
        style={{
          backgroundColor: theme['border-basic-color-2'],
          width: 34,
          height: 34,
          fontSize: 10,
          color:
            theme[
              isDarkTheme() ? 'border-alternative-color-4' : 'color-basic-1100'
            ],
          textAlign: 'center',
        }}
        value={`${value}`}
        onChangeText={(text) => setValue(Number(text))}
        keyboardType="numeric"
        placeholderTextColor={
          theme[
            isDarkTheme() ? 'border-alternative-color-4' : 'color-basic-1100'
          ]
        }
      />
      <QtyPlusButton
        backgroundColor={theme['border-basic-color-3']}
        onPress={() => setValue(Number(value) + 1)}>
        <QuantityIcon name="plus-outline" fill={theme['text-hint-color']} />
      </QtyPlusButton>
    </View>
  );
};
