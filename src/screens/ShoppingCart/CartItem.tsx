/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { Avatar, Layout, Text, useTheme } from '@ui-kitten/components';
import { Dimensions, TouchableOpacity } from 'react-native';
import { CenteredView } from '@/components/StyledComponents';
import { Quantity, Price } from '@/components';
import { ICartItem as ProductModel } from '@/models/ICartItem';
import { useDispatch } from 'react-redux';
import { removeCartItem, updateCartItem } from '@/redux/ducks/cartDuck';
import useModal from '@/hooks/useModal';
import { getProductFullPrice } from '@/utils';

type Props = {
  product: ProductModel;
  onPress();
};

const Product: React.FC<Props> = ({ product, onPress }) => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { showQuestion } = useModal();
  const deleteFromCart = () => {
    dispatch(removeCartItem(product));
  };

  const setQuantity = (qty: number) => {
    if (qty < 1) {
      return showQuestion(
        `Deseja remover o produto "${product.name}" do carrinho?`,
        () => deleteFromCart(),
        () => {},
      );
    }
    dispatch(updateCartItem({ ...product, quantity: qty }));
  };

  return (
    <TouchableOpacity onPress={onPress}>
      <CenteredView
        row
        extraStyles={`width: ${
          Dimensions.get('screen').width - 32
        }px; justify-content: flex-start;`}>
        <Avatar
          shape="square"
          size="giant"
          source={{ uri: product.image }}
          style={{ height: 64, width: 64 }}
        />
        <Layout style={{ marginLeft: 8 }}>
          <Text category="p1">{product.name}</Text>
          {product.variations && product.variations.length
            ? product.variations.map((variation) => (
                <Text category="p2" style={{ color: theme['color-basic-600'] }}>
                  {variation.name}: {variation.selectedOption.value}
                </Text>
              ))
            : undefined}
        </Layout>
        <Layout style={{ marginLeft: 'auto' }}>
          <CenteredView>
            <Price
              style={{ marginBottom: 16, fontSize: 18 }}
              value={getProductFullPrice(
                product,
                // @ts-ignore
                product.selectedVariationsOptions,
              )}
            />
            <Quantity
              style={{}}
              value={product.quantity}
              setValue={(q) => setQuantity(q)}
            />
          </CenteredView>
        </Layout>
      </CenteredView>
    </TouchableOpacity>
  );
};

export default Product;
