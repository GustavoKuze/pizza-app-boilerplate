import { View } from 'react-native';
import styled from 'styled-components/native';

export const ToggleContainer = styled(View)`
  margin-top: 16px;
  align-items: center;
  justify-content: space-between;
  flex-direction: row;
`;
