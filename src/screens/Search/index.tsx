import React from 'react';
import { SafeAreaView } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles from './styles';
import { Divider } from '@ui-kitten/components';
import { TitleBar } from '@/components';
import SearchScreen from './SearchScreen';

type RootStackParamList = {
  Search: undefined;
  Settings: undefined;
};

type Props = {
  route: RouteProp<RootStackParamList, 'Search'>;
  navigation: StackNavigationProp<RootStackParamList, 'Search'>;
};

export const Search: React.FC<Props> = ({ route, navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <TitleBar title={'Pesquisar'} />
      <Divider />
      <SearchScreen navigation={navigation} />
    </SafeAreaView>
  );
};
