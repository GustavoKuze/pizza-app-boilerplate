import { put, call } from 'redux-saga/effects';
import { listOrders } from '@/services/order';
import { types as orderTypes } from '@/redux/ducks/orderDuck';

function* fetchOrders(action: any) {
  yield put({
    type: orderTypes.SET_LOADING,
    payload: true,
  });

  const { userId, onSuccess, onError } = action.payload;

  const result = yield call(() => listOrders(userId));

  if (!result.error) {
    yield put({
      type: orderTypes.SET_ORDERS,
      payload: result,
    });
    if (typeof onSuccess === 'function') {
      onSuccess();
    }
  } else {
    if (typeof onError === 'function') {
      onError(result.error);
    }
  }

  yield put({
    type: orderTypes.SET_LOADING,
    payload: false,
  });
}

export { fetchOrders };
