export type VariationOption = {
  key: string;
  value: string;
};

export type CartItemVariation = {
  name: string;
  selectedOption: VariationOption;
};

export type CartItemSelectedVariationOption = {
  id: number;
  key: string;
  productVariationId: number;
};

export interface ICartItem {
  id: number;
  image: string;
  name: string;
  description: string;
  observations?: string;
  quantity: number;
  price: number;
  variations?: CartItemVariation[];
  selectedVariationsOptions?: CartItemSelectedVariationOption[] | undefined;
}
