import { ICreditCard } from '@/models/ICreditCard';
import { request } from './api';

const listUserCreditCards = async (userId: number): Promise<ICreditCard[]> => {
  try {
    const response = await request(`/credit-card/${userId}`);

    return response.data.data;
  } catch (err) {
    console.log(err);
    return [];
  }
};

const createCreditCard = async (creditCard: ICreditCard): Promise<any> => {
  return request('/credit-card', 'POST', creditCard);
};

const updateCreditCard = async (
  userId: number,
  creditCard: ICreditCard,
): Promise<any> => {
  return request(`/credit-card/${userId}`, 'PUT', creditCard);
};

const deleteCreditCard = async (creditCardId: number): Promise<any> => {
  return request(`/credit-card/${creditCardId}`, 'DELETE');
};

export {
  listUserCreditCards,
  createCreditCard,
  updateCreditCard,
  deleteCreditCard,
};
