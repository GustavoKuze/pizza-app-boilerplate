import { Avatar } from '@ui-kitten/components';
import styled from 'styled-components/native';

export const UserAvatar = styled(Avatar)`
  margin: 0px;
  padding: 0px;
  width: ${({ customSize }) => customSize || 32}px;
  height: ${({ customSize }) => customSize || 32}px;
  margin-right: 8px;
`;
