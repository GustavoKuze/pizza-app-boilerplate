type Data = {
  id: number;
  name: string;
  email: string;
  phone: string;
  disabled: string;
  created_at: string;
  updated_at: string;
  password: string;
  avatar: string;
  username: string;
};

export interface ISignUpResult {
  data?: Data;
  error?: string;
}
