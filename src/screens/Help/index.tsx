import React, { useState } from 'react';
import { SafeAreaView, ToastAndroid } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles from './styles';
import { Button, TitleBar } from '@/components';
import {
  IndexPath,
  Input,
  Select,
  SelectGroup,
  SelectItem,
  Text,
} from '@ui-kitten/components';
import { Body } from '@sc/index';

const data = [
  {
    title: 'Tirar foto',
    items: [
      'Reconhecer uma espécie',
      'Criar uma postagem',
      'Editar uma postagem',
    ],
  },
  {
    title: 'Perfil',
    items: ['Alteração de senha', 'Alteração de e-mail', 'Foto de perfil'],
  },
];

type RootStackParamList = {
  Help: undefined;
  Settings: undefined;
};

type Props = {
  route: RouteProp<RootStackParamList, 'Help'>;
  navigation: StackNavigationProp<RootStackParamList, 'Help'>;
};

export const Help: React.FC<Props> = ({ navigation }) => {
  const [description, setDescription] = useState('');
  const [reason, setReason] = useState(data[0].items[0]);
  const [selectedIndex, setSelectedIndex] = React.useState(new IndexPath(0, 1));

  const [reasonInputStatus, setReasonInputStatus] = useState('error');
  const [descriptionInputStatus, setDescriptionInputStatus] = useState('error');

  const validateFields = (): Boolean => {
    setReasonInputStatus(reason ? 'success' : 'danger');
    setDescriptionInputStatus(description ? 'success' : 'danger');

    return !!description && !!reason;
  };

  const sendHelpRequest = () => {
    if (!validateFields()) {
      return;
    }

    ToastAndroid.show(`Motivo: ${reason}`, ToastAndroid.SHORT);
    navigation.goBack();
  };

  return (
    <SafeAreaView style={styles.container}>
      <TitleBar title={'Ajuda'} onGoBack={() => navigation.goBack()} />
      <Body>
        <Text category="S2">Envie aqui sua dúvida</Text>

        <Select
          size="medium"
          label="Escolha o motivo"
          style={styles.select}
          placeholder="Selecione o motivo"
          status={reasonInputStatus}
          selectedIndex={selectedIndex}
          value={reason}
          // @ts-ignore
          onSelect={(index) => {
            // @ts-ignore
            setSelectedIndex(index);
            // @ts-ignore
            setReason(data[index.section].items[index.row]);
          }}>
          {data.map((group) => (
            <SelectGroup key={group.title} title={group.title}>
              {group.items.map((item) => (
                <SelectItem key={item} title={item} />
              ))}
            </SelectGroup>
          ))}
        </Select>

        <Input
          size="medium"
          placeholder="Descreva sua dúvida ou problema"
          label="Descrição"
          multiline
          textStyle={{
            minHeight: 64,
          }}
          value={description}
          onChangeText={(nextValue) => setDescription(nextValue)}
          status={descriptionInputStatus}
          onBlur={validateFields}
        />

        <Button
          onPress={() => {
            sendHelpRequest();
          }}
          status="info"
          style={{ marginTop: 16, marginHorizontal: 58 }}>
          Enviar
        </Button>
      </Body>
    </SafeAreaView>
  );
};
