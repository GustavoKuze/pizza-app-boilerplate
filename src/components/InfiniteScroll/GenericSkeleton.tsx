import React from 'react';
import { View } from 'react-native';
import Shimmer from 'react-native-shimmer';

const GenericSkeleton: React.FC = () => {
  return (
    <View style={{ marginVertical: 16 }}>
      <View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            marginHorizontal: 32,
          }}>
          <Shimmer animationOpacity={0.8} duration={2000}>
            <View
              style={{
                backgroundColor: '#DDD',
                width: 28,
                height: 28,
                borderRadius: 100,
              }}
            />
          </Shimmer>
          <View
            style={{
              marginLeft: 20,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <View style={{ flex: 1 }}>
              <Shimmer
                animationOpacity={0.8}
                duration={2000}
                style={{
                  backgroundColor: '#DDD',
                  width: 140,
                  height: 20,
                  borderRadius: 4,
                }}
              />
            </View>
            <View
              style={{
                flex: 1,
                flexDirection: 'column',
                alignItems: 'flex-end',
              }}>
              <Shimmer
                animationOpacity={0.8}
                duration={2000}
                style={{
                  backgroundColor: '#DDD',
                  width: 16,
                  height: 12,
                  marginRight: 16,
                }}
              />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default GenericSkeleton;
