const colors = {
  primary: '#7DB307',
  success: '#40CE50',
  info: '#00ADE2',
  warning: '#F9B300',
  danger: '#FF4D26',
  black: 'black',
  white: 'white',
  divider: 'rgba(0, 0, 0, 0.07)',
  modalBackdrop: 'rgba(0, 0, 0, 0.5)',
};

const paymentMethodsFriendlyLabels = {
  credit: 'Cartão de crédito',
  debit: 'Cartão de débito',
  pix: 'PIX',
  money: 'Pagar na retirada',
};

export { colors, paymentMethodsFriendlyLabels };
