import { IAddress } from '@/models/IAddress';
import { IAPIResponse } from '@/models/IAPIResponse';
import { request } from './api';

const listUserAddresses = async (
  userId: number,
): Promise<IAddress[] | IAPIResponse> => {
  try {
    const response = await request(`/address/${userId}`);

    if (!response.ok) {
      return response;
    }

    return response.data.data;
  } catch (err) {
    console.log(err);
    return [];
  }
};

const createAddress = async (address: IAddress): Promise<any> => {
  return request('/address', 'POST', address);
};

const updateAddress = async (
  userId: number,
  address: IAddress,
): Promise<any> => {
  return request(`/address/${userId}`, 'PUT', address);
};

const deleteAddress = async (addressId: number): Promise<any> => {
  return request(`/address/${addressId}`, 'DELETE');
};

const getAddressByCEP = async (cep: string): Promise<any> => {
  return request(`/address/cep/${cep}`);
};

export {
  listUserAddresses,
  createAddress,
  updateAddress,
  deleteAddress,
  getAddressByCEP,
};
