import React, { useRef, useState } from 'react';
import { SafeAreaView, ToastAndroid } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles from './styles';
import { TitleBar, Button } from '@/components';
import { Input } from '@ui-kitten/components';
import { Body } from '@sc/index';
import { generateShadow } from '@/utils';

type RootStackParamList = {
  ForgetPassword: undefined;
  ChangePassword: undefined;
};

type Props = {
  route: RouteProp<RootStackParamList, 'ForgetPassword'>;
  navigation: StackNavigationProp<RootStackParamList, 'ForgetPassword'>;
};

export const ForgetPassword: React.FC<Props> = ({ navigation }) => {
  const [email, setEmail] = useState('');

  const [emailInputStatus, setEmailInputStatus] = useState('error');

  const emailInput = useRef<any>();

  const validateFields = (): Boolean => {
    setEmailInputStatus(email ? 'success' : 'danger');

    return !!email;
  };

  const sendRecoveryEmail = () => {
    if (!validateFields()) {
      return;
    }

    ToastAndroid.show('Nova senha salva', ToastAndroid.SHORT);
    navigation.navigate('ChangePassword');
  };

  return (
    <SafeAreaView style={styles.container}>
      <TitleBar
        title={'Esqueci a senha'}
        onGoBack={() => navigation.goBack()}
      />
      <Body>
        <Input
          style={{ ...generateShadow(3) }}
          ref={emailInput}
          placeholder="Digite seu e-mail"
          label="Qual é o seu e-email?"
          value={email}
          onChangeText={(nextValue) => setEmail(nextValue)}
          status={emailInputStatus}
          onBlur={validateFields}
          onSubmitEditing={() => {
            sendRecoveryEmail();
          }}
        />
        <Button
          onPress={() => {
            sendRecoveryEmail();
          }}
          status="primary"
          style={{
            marginTop: 16,
            marginHorizontal: 58,
            ...generateShadow(2),
          }}>
          Enviar
        </Button>
      </Body>
    </SafeAreaView>
  );
};
