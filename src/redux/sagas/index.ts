import { takeLatest, all } from 'redux-saga/effects';
import { types as creditCardTypes } from '../ducks/creditCardDuck';
import { fetchCreditCards } from './creditCardSaga';
import { types as addressTypes } from '../ducks/addressDuck';
import { fetchAddresses } from './addressSaga';
import { types as orderTypes } from '../ducks/orderDuck';
import { fetchOrders } from './orderSaga';
import { types as menuTypes } from '../ducks/menuDuck';
import { fetchMenu } from './menuSaga';
import { types as searchTypes } from '../ducks/searchDuck';
import { searchProductSaga } from './searchSaga';
import { types as userTypes } from '../ducks/userDuck';
import { postLogin, postSignup } from './userSaga';

export default function* root() {
  yield all([
    yield takeLatest(creditCardTypes.GET_CREDITCARDS_ASYNC, fetchCreditCards),
    yield takeLatest(addressTypes.GET_ADDRESSES_ASYNC, fetchAddresses),
    yield takeLatest(orderTypes.GET_ORDERS_ASYNC, fetchOrders),
    yield takeLatest(menuTypes.GET_MENU_ASYNC, fetchMenu),
    yield takeLatest(searchTypes.SEARCH_PRODUCT_ASYNC, searchProductSaga),
    yield takeLatest(userTypes.LOGIN_ASYNC, postLogin),
    yield takeLatest(userTypes.SIGNUP_ASYNC, postSignup),
  ]);
}
