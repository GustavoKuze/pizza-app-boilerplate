/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { Avatar, Layout, Text, useTheme } from '@ui-kitten/components';
import { Dimensions, TouchableOpacity } from 'react-native';
import { CenteredView } from '@/components/StyledComponents';
import { Product as ProductModel } from '@/models/Product';

// import { Container } from './styles';

type Props = {
  product: ProductModel;
  onPress();
};

const Product: React.FC<Props> = ({ product, onPress }) => {
  const theme = useTheme();

  return (
    <TouchableOpacity onPress={onPress}>
      <CenteredView
        row
        extraStyles={`width: ${
          Dimensions.get('screen').width - 100
        }px; justify-content: flex-start;margin-top: 16px;`}>
        <Avatar
          shape="square"
          size="giant"
          source={{ uri: product.image }}
          style={{ height: 64, width: 64 }}
        />
        <Layout style={{ marginLeft: 8 }}>
          <Text category="p1">{product.name}</Text>
          <Text category="p2" style={{ color: theme['color-basic-600'] }}>
            {product.description}
          </Text>
        </Layout>
      </CenteredView>
    </TouchableOpacity>
  );
};

export default Product;
