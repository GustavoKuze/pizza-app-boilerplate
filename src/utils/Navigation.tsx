import * as React from 'react';

export const navigationRef = React.createRef();

export function navigate(name, params) {
  // @ts-ignore
  navigationRef.current?.navigate(name, params);
}

export function replace(name, params) {
  // @ts-ignore
  navigationRef.current?.replace(name, params);
}

export function popToTop() {
  // @ts-ignore
  navigationRef.current?.popToTop();
}

export function goBack() {
  // @ts-ignore
  navigationRef.current?.goBack();
}
