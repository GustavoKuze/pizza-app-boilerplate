/* eslint-disable react-hooks/exhaustive-deps */
import { Layout, useTheme } from '@ui-kitten/components';
import React, { useEffect, useState } from 'react';
import { Dimensions } from 'react-native';
import {
  AutoCompleteItem,
  AutoCompleteInput as AutoComplete,
} from '@/components/AutoComplete';
import CenteredView from '@sc/CenteredView';
import { SearchPlaceholderImage } from './styles';
import { useDispatch, useSelector } from 'react-redux';
import { searchProductAsync } from '@/redux/ducks/searchDuck';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import { debounce, generateShadow } from '@/utils';
import { Text } from 'react-native';

type RootStackParamList = {
  SearchScreen: undefined;
  Home: undefined;
  ProductDetails: { productId: number };
};

type Props = {
  route: RouteProp<RootStackParamList, 'SearchScreen'>;
  navigation: StackNavigationProp<RootStackParamList, 'SearchScreen'>;
};

const SearchScreen: React.FC<Props> = ({ navigation }) => {
  const theme = useTheme();
  const dispatch = useDispatch();
  const { products, isLoading } = useSelector((state: any) => state.search);
  const [search, setSearch] = useState('');

  useEffect(() => {
    dispatch(searchProductAsync(search));
  }, [search]);

  return (
    <Layout style={{ flex: 1, alignItems: 'center' }}>
      <AutoComplete
        loading={isLoading}
        style={{
          width: Dimensions.get('window').width - 58,
          marginTop: 16,
          ...generateShadow(5),
        }}
        items={(products || []).map(
          (product) =>
            new AutoCompleteItem(
              product.id,
              product.name,
              product.description,
              product.image,
            ),
        )}
        onChange={debounce((text) => setSearch(text || ''), 350)}
        onSelect={(productName, product) => {
          navigation.navigate('ProductDetails', { productId: product.id });
          setSearch('');
        }}
      />
      <SearchPlaceholderImage
        size="giant"
        source={require('@/assets/screen_placeholder.png')}
      />

      <CenteredView
        style={{
          marginTop: -80,
          marginLeft: 'auto',
          marginRight: Dimensions.get('screen').width / 4,
        }}>
        <Text
          style={{
            fontFamily: 'Caveat-Regular',
            fontSize: 28,
            color: theme['color-primary-400'],
          }}>
          Tá com
        </Text>
        <Text
          style={{
            fontFamily: 'Caveat-Bold',
            fontSize: 28,
            color: theme['color-primary-400'],
          }}>
          FOME{' '}
        </Text>
        <Text
          style={{
            fontFamily: 'Caveat-Regular',
            fontSize: 28,
            color: theme['color-primary-400'],
          }}>
          de que?{' '}
        </Text>
      </CenteredView>
    </Layout>
  );
};

export default SearchScreen;
