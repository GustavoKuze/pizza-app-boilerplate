import AsyncStorage from '@react-native-async-storage/async-storage';

export const loadState = async (keyName = 'pizza_global_state') => {
  try {
    const state = await AsyncStorage.getItem(keyName);

    if (state !== null && Object.keys(state).length > 0) {
      return JSON.parse(state);
    }
    return undefined;
  } catch (error) {
    return Promise.reject(error);
  }
};

export const saveState = async (state, keyName = 'pizza_global_state') => {
  const stateJson = JSON.stringify(state);

  try {
    return await AsyncStorage.setItem(keyName, stateJson);
  } catch (error) {
    return Promise.reject(error);
  }
};

export const deleteState = async (keyName) => {
  try {
    if (keyName) {
      return await AsyncStorage.removeItem(keyName);
    }
    return await AsyncStorage.clear();
  } catch (err) {
    return Promise.reject(err);
  }
};
