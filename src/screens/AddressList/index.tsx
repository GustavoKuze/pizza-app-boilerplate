import React, { useState } from 'react';
import {
  SafeAreaView,
  TouchableOpacity,
  ActivityIndicator,
  Dimensions,
} from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp, useFocusEffect } from '@react-navigation/native';
import styles from './styles';
import { TitleBar, TouchableIcon, Button } from '@/components';
import { Layout, Text, useTheme, Divider, Icon } from '@ui-kitten/components';
import { Body, CenteredView } from '@sc/index';
import { FlatList } from 'react-native-gesture-handler';
import { generateShadow } from '@/utils';
import { useDispatch, useSelector } from 'react-redux';
import { getAddressesAsync } from '@/redux/ducks/addressDuck';
import { IAddress } from '@/models/IAddress';
import { deleteAddress } from '@/services/address';
import useModal from '@/hooks/useModal';
import { setAddress } from '@/redux/ducks/cartDuck';

type RootStackParamList = {
  AddressList: { onSelect?: Function | undefined };
  EditAddress: { address: IAddress | undefined };
};

type Props = {
  route: RouteProp<RootStackParamList, 'AddressList'>;
  navigation: StackNavigationProp<RootStackParamList, 'AddressList'>;
};

export const AddressList: React.FC<Props> = ({ navigation, route }) => {
  const {
    cart: { address: cartAddress },
  } = useSelector((state: any) => state.cart);
  const [selectedAddressId, setSelectedAddressId] = useState(
    cartAddress.id || 0,
  );
  const { userId } = useSelector((state: any) => state.user);
  const { addresses }: { addresses: IAddress[] } = useSelector(
    (state: any) => state.address,
  );
  const [isLoading, setIsLoading] = useState(true);
  const theme = useTheme();
  const dispatch = useDispatch();
  const { showQuestion } = useModal();

  useFocusEffect(
    React.useCallback(() => {
      setIsLoading(true);
      dispatch(
        getAddressesAsync(
          userId,
          () => {
            setIsLoading(false);
          },
          () => {
            setIsLoading(false);
          },
        ),
      );
    }, []),
  );

  const renderBtnCreateAddress = () => (
    <Button
      onPress={() => {
        navigation.navigate('EditAddress', { address: undefined });
      }}
      status="info"
      style={{ marginHorizontal: 58, marginTop: 8 }}>
      Cadastrar Endereço
    </Button>
  );

  const isAddressSelected = (addressId) => selectedAddressId === addressId;

  const callConfirmDelete = async (address: IAddress) => {
    showQuestion(
      address.name
        ? `Tem certeza de que deseja excluir o endereço "${address.name}?"`
        : 'Tem certeza de que deseja excluir este endereço?',
      async () => {
        setIsLoading(true);
        await deleteAddress(address.id);

        if (isAddressSelected(address.id)) {
          // @ts-ignore
          dispatch(setAddress({}));
          setSelectedAddressId(0);
        }

        dispatch(
          getAddressesAsync(
            userId,
            () => {
              setIsLoading(false);
            },
            () => {
              setIsLoading(false);
            },
          ),
        );
      },
      () => {},
    );
  };

  return (
    <SafeAreaView style={styles.container}>
      <TitleBar
        title={'Meus Endereços'}
        // onGoBack={
        //   typeof route.params?.onSelect !== 'function'
        //     ? () => navigation.goBack()
        //     : undefined
        // }
        onGoBack={() => navigation.goBack()}
      />

      <Divider />

      <Body style={{ paddingBottom: 32 }}>
        {isLoading ? (
          <ActivityIndicator
            style={{ marginTop: Dimensions.get('screen').height / 3 }}
            size={64}
            color={theme['color-primary-500']}
          />
        ) : (
          <>
            {addresses.length > 0 ? (
              <>
                {typeof route.params?.onSelect === 'function' && (
                  <Text
                    category="s2"
                    style={{ fontSize: 18, marginLeft: 8, marginBottom: 16 }}>
                    Selecione o endereço de entrega
                  </Text>
                )}
                <FlatList
                  contentContainerStyle={{ paddingVertical: 16 }}
                  data={addresses}
                  renderItem={({ item }) => (
                    <TouchableOpacity
                      onPress={() =>
                        typeof route.params?.onSelect === 'function'
                          ? setSelectedAddressId(item.id)
                          : navigation.navigate('EditAddress', {
                              address: item,
                            })
                      }>
                      <Layout
                        style={{
                          marginHorizontal: 8,
                          marginVertical: 8,
                          padding: item.id === selectedAddressId ? 13 : 16,
                          flexDirection: 'row',
                          justifyContent: 'space-between',
                          alignItems: 'center',
                          borderRadius: 12,
                          borderWidth: item.id === selectedAddressId ? 3 : 0,
                          borderColor: theme['color-primary-500'],
                          ...generateShadow(6),
                        }}>
                        <Layout>
                          {item.name ? <Text>{item.name}</Text> : undefined}
                          <Text>{item.complement}</Text>
                          <Text>{item.cep}</Text>
                          <Text>{item.street}</Text>
                          <Text>{item.neighborhood}</Text>
                          <Text>{item.number}</Text>
                          <Text>{item.city}</Text>
                          <Text>{item.state}</Text>
                        </Layout>
                        <Layout>
                          <TouchableIcon
                            size={24}
                            onPress={() => callConfirmDelete(item)}
                            name="trash-2-outline"
                            color={theme['color-danger-500']}
                            style={{ marginBottom: 32 }}
                          />
                          <TouchableIcon
                            size={24}
                            onPress={() => {
                              navigation.navigate('EditAddress', {
                                address: item,
                              });
                            }}
                            name="edit-outline"
                            color={theme['color-warning-500']}
                          />
                        </Layout>
                      </Layout>
                    </TouchableOpacity>
                  )}
                  keyExtractor={(dataItem) => `${dataItem.id}`}
                  refreshing={false}
                  initialNumToRender={40}
                  maxToRenderPerBatch={40}
                />
                <Divider />

                {renderBtnCreateAddress()}
                {typeof route.params?.onSelect === 'function' && (
                  <Button
                    disabled={!selectedAddressId}
                    onPress={() => {
                      // @ts-ignore
                      route.params?.onSelect(
                        addresses.find(({ id }) => id === selectedAddressId),
                      );
                    }}
                    status="primary"
                    style={{ marginHorizontal: 58, marginTop: 12 }}>
                    Continuar
                  </Button>
                )}
              </>
            ) : (
              <CenteredView>
                <Icon
                  fill={theme['text-hint-color']}
                  style={{
                    width: Dimensions.get('screen').width / 3,
                    height: Dimensions.get('screen').width / 3,
                    marginTop: '20%',
                  }}
                  name="pin-outline"
                />
                <Text
                  category="h5"
                  style={{
                    color: theme['text-hint-color'],
                    textAlign: 'center',
                    marginBottom: 8,
                  }}>
                  Você ainda não tem nenhum endereço
                </Text>
                {renderBtnCreateAddress()}
              </CenteredView>
            )}
          </>
        )}
      </Body>
    </SafeAreaView>
  );
};
