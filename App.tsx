import React from 'react';
import * as eva from '@eva-design/eva';
import { ApplicationProvider, IconRegistry } from '@ui-kitten/components';
import { EvaIconsPack } from '@ui-kitten/eva-icons';
import { AppNavigator } from '@/config/navigation';
import { Provider } from 'react-redux';
import { GoogleSignin } from '@react-native-google-signin/google-signin';

GoogleSignin.configure({
  webClientId:
    '747090822172-g7ssj9uqfau85fsl47sjpfnbul83fnig.apps.googleusercontent.com',
  offlineAccess: true,
  scopes: ['profile', 'email'],
});

// @ts-ignore
import customTheme from '@/assets/custom-theme.json';
import configureStore from '@/redux/configureStore';
import { Modal } from '@/components';
import StateLoader from '@/components/StateLoader';
import { isDarkTheme } from '@/utils';

export default () => (
  <Provider store={configureStore}>
    <StateLoader />
    <IconRegistry icons={EvaIconsPack} />
    <ApplicationProvider
      {...eva}
      theme={{ ...eva[isDarkTheme() ? 'dark' : 'light'], ...customTheme }}>
      <Modal />
      <AppNavigator />
    </ApplicationProvider>
  </Provider>
);
