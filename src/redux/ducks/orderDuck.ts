const typesPrefix = '@order';

export const types = {
  SET_LOADING: `${typesPrefix}/SET_LOADING`,
  GET_ORDERS_ASYNC: `${typesPrefix}/GET_ORDERS_ASYNC`,
  SET_ORDERS: `${typesPrefix}/SET_ORDERS`,
};

const INITIAL_STATE: {
  orders: any[];
  isLoading: Boolean;
} = {
  orders: [],
  isLoading: false,
};

export default (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case types.SET_ORDERS:
      return { ...state, orders: action.payload };
    case types.SET_LOADING:
      return { ...state, isLoading: action.payload };
    default:
      return state;
  }
};

export const setOrders = (orders: any) => ({
  type: types.SET_ORDERS,
  payload: orders,
});

export const getOrdersAsync = (
  userId: number,
  onSuccess = () => {},
  onError = () => {},
) => ({
  type: types.GET_ORDERS_ASYNC,
  payload: {
    userId,
    onSuccess,
    onError,
  },
});

export const setIsLoading = (isLoading: Boolean) => ({
  type: types.SET_LOADING,
  payload: isLoading,
});

export const allActions = {
  getOrdersAsync,
  setOrders,
  setIsLoading,
};
