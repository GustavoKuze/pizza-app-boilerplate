import React from 'react';
import { SafeAreaView, View } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles from './styles';
import { Button, TitleBar } from '@/components';
import { Body } from '@sc/index';
import { Divider, Text } from '@ui-kitten/components';
import { colors } from '@/constants';

type RootStackParamList = {
  ChangePassword: undefined;
  AccountSettings: undefined;
};

type Props = {
  route: RouteProp<RootStackParamList, 'AccountSettings'>;
  navigation: StackNavigationProp<RootStackParamList, 'AccountSettings'>;
};

export const AccountSettings: React.FC<Props> = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <TitleBar title={'Conta'} onGoBack={() => navigation.goBack()} />
      <Divider />
      <Body>
        <Text>E-mail</Text>
        <Text category="label">fulano@gmail.com</Text>
        <Divider style={{ backgroundColor: colors.divider, marginTop: 16 }} />
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'space-between',
            flexDirection: 'row',
          }}>
          <View>
            <Text style={{ marginTop: 16 }}>Senha</Text>
            <Text category="label">********</Text>
          </View>
          <Button
            onPress={() => {
              navigation.navigate('ChangePassword');
            }}
            appearance="ghost"
            status="info">
            Editar senha
          </Button>
        </View>
        <Divider style={{ backgroundColor: colors.divider, marginTop: 16 }} />
      </Body>
    </SafeAreaView>
  );
};
