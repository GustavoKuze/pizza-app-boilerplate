export interface ISearchProducts {
  id: number;
  name: string;
  description: string;
  price: number;
  image: string;
  createdAt: Date;
  updatedAt: Date;
  enabled: boolean;
}
