import React, { useEffect, useState } from 'react';
import {
  Dimensions,
  SafeAreaView,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles, { ProductName, ProductDescription } from './styles';
import { TitleBar, ButtonGroup, Quantity, Price, Button } from '@/components';
import { Avatar, useTheme, Input } from '@ui-kitten/components';
import { Body, CenteredView } from '@sc/index';
import { Product } from '@/models/Product';
import { getProductDetails } from '@/services/menu';
import { useDispatch, useSelector } from 'react-redux';
import { addCartItem, updateCartItem } from '@/redux/ducks/cartDuck';
import { getProductFullPrice } from '@/utils';

type RootStackParamList = {
  ProductDetails: {
    productId: number;
  };
  Home: any;
};

type Props = {
  route: RouteProp<RootStackParamList, 'ProductDetails'>;
  navigation: StackNavigationProp<RootStackParamList, 'ProductDetails'>;
};

export const ProductDetails: React.FC<Props> = ({ navigation, route }) => {
  const dispatch = useDispatch();
  const {
    cart: { address, payment, user, coupons, deliveryFee, items },
    isLoading: isLoadingCart,
  } = useSelector((state: any) => state.cart);

  const theme = useTheme();

  // @ts-ignore
  const [product, setProduct] = useState<Product>({});

  const [variationsStates, setVariationsStates] = useState<any>([]);
  const [observations, setObservations] = useState('');
  const [quantity, setQuantity] = useState(1);
  const [isLoading, setIsLoading] = useState(true);

  const setSelectedVariationOption = (
    variationName: string,
    selected: number,
  ) => {
    setVariationsStates(
      variationsStates.map((variation) =>
        variation.name === variationName
          ? { ...variation, selected }
          : variation,
      ),
    );
  };

  const findCartItem = (productId) => items.find(({ id }) => id === productId);

  const loadCartItemDetails = (productDetails) => {
    const cartItem = findCartItem(route.params.productId);
    if (cartItem) {
      setObservations(cartItem.observations);
      setQuantity(cartItem.quantity);

      setVariationsStates(
        !!productDetails.variations && productDetails.variations.length > 0
          ? productDetails.variations.map((variation) => ({
              name: variation.name,
              selected: variation.options.findIndex(
                ({ id }) =>
                  id ===
                  cartItem.variations.find(
                    ({ name }) => name === variation.name,
                  ).selectedOption.id,
              ),
            }))
          : [],
      );
    }
  };

  const callGetProductDetails = async () => {
    setIsLoading(true);
    const productDetails = await getProductDetails(route.params.productId);

    setProduct(productDetails);
    setVariationsStates(
      !!productDetails.variations && productDetails.variations.length > 0
        ? productDetails.variations.map((variation) => ({
            name: variation.name,
            selected: 0,
          }))
        : [],
    );

    loadCartItemDetails(productDetails);

    setIsLoading(false);
  };

  useEffect(() => {
    callGetProductDetails();
  }, []);

  const getSelectedVariationsOptions = () => {
    return variationsStates.map(({ name, selected }) => {
      return product.variations?.find(({ name: vName }) => vName === name)
        ?.options[selected];
    });
  };

  const addItemToCart = () => {
    const selectedVariationsOptions = getSelectedVariationsOptions();

    const variations = variationsStates.map(({ name, selected }) => {
      const variation = product.variations?.find(
        ({ name: vName }) => vName === name,
      );
      const selectedOption = variation?.options[selected];
      return { name, selectedOption };
    });

    const action = findCartItem(product.id) ? updateCartItem : addCartItem;

    dispatch(
      action({
        id: product.id,
        description: product.description,
        image: product.image,
        name: product.name,
        price: product.price,
        observations,
        quantity,
        selectedVariationsOptions,
        variations,
      }),
    );

    navigation.navigate('Home', { screen: 'ShoppingCart' });
  };

  return (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: theme['background-basic-color-1'],
      }}>
      <TitleBar title={''} onGoBack={() => navigation.goBack()} />

      {isLoading ? (
        <ActivityIndicator
          style={{ marginTop: Dimensions.get('screen').height / 3 }}
          size={64}
          color={theme['color-primary-500']}
        />
      ) : (
        <ScrollView>
          <Body style={{ paddingBottom: 58 }}>
            <CenteredView>
              <Avatar
                source={{ uri: product.image }}
                size="giant"
                shape="square"
                style={{
                  width: Dimensions.get('window').width / 1.3,
                  height: Dimensions.get('window').width / 2,
                }}
              />
              <ProductName>{product.name}</ProductName>
              <ProductDescription>{product.description}</ProductDescription>
              <Price
                style={{ marginBottom: 16 }}
                value={getProductFullPrice(
                  product,
                  getSelectedVariationsOptions(),
                )}
              />
            </CenteredView>

            {product.variations && product.variations.length > 0
              ? product.variations.map((variation) => (
                  <ButtonGroup
                    style={{ marginBottom: 16 }}
                    variation={variation}
                    onSelect={(selectedIndex) =>
                      setSelectedVariationOption(variation.name, selectedIndex)
                    }
                    selected={
                      variationsStates.find(
                        ({ name }) => name === variation.name,
                      )?.selected || 0
                    }
                  />
                ))
              : undefined}

            <Input
              size="medium"
              placeholder="E.x: Sem cebola, pouco sal, etc..."
              label="Observações"
              multiline
              style={{ marginBottom: 16 }}
              textStyle={{
                minHeight: 64,
              }}
              value={observations}
              onChangeText={(nextValue) => setObservations(nextValue)}
            />

            <CenteredView row>
              <Quantity
                value={quantity}
                setValue={(q) => (q > 0 ? setQuantity(q) : undefined)}
              />
              <Button
                onPress={addItemToCart}
                status="primary"
                style={{ marginLeft: 16, height: 34, paddingVertical: 0 }}>
                {findCartItem(product.id)
                  ? 'Atualizar no carrinho'
                  : 'Adicionar ao carrinho'}
              </Button>
            </CenteredView>
          </Body>
        </ScrollView>
      )}
    </SafeAreaView>
  );
};
