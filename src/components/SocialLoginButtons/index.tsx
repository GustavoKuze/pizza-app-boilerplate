import React, { useEffect } from 'react';

import { createEvaIcon } from '@/utils/layout';
import auth from '@react-native-firebase/auth';
import {
  GoogleSignin,
  statusCodes,
} from '@react-native-google-signin/google-signin';
import { setLoginData } from '@/redux/ducks/userDuck';

import {
  // FacebookButton,
  GoogleButton,
} from './styles';
import { generateShadow } from '@/utils';
import { useDispatch } from 'react-redux';
import { socialLogin } from '@/services/auth';

export const SocialLoginButtons: React.FC = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged((user) => {
      console.log('Google login user:');
      console.log(user);
    });
    return subscriber; // unsubscribe on unmount
  }, []);

  const signInWithGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices({
        showPlayServicesUpdateDialog: true,
      });

      const { idToken: firstIdToken } = await GoogleSignin.signIn();

      const googleCredential = auth.GoogleAuthProvider.credential(firstIdToken);

      const { user } = await auth().signInWithCredential(googleCredential);

      const idToken = await user.getIdToken(true);

      const socialLoginResult = await socialLogin(
        user.uid,
        user.email || '',
        user.providerId,
        user.displayName || '',
      );

      if (socialLoginResult.error) {
        throw new Error('Não foi possível obter os usuários');
      }

      dispatch(
        setLoginData({
          ...socialLoginResult.data,
          userId: socialLoginResult.data.id,
          currentAuthority: 'user',
          email: user.email || '',
          token: idToken,
          //@ts-ignore
          ...user._user,
        }),
      );
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
        console.log('Google login cancelled by user');
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
        console.log(
          'Google login in progress. Cannot login twice at same time...',
        );
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
        console.error('play services not available or outdated');
      } else {
        // some other error happened
        console.log('Error while trying to login with Google auth:');
        console.error(error);
      }
    }
    try {
      auth().signOut();
    } catch (er) {}
  };

  return (
    <>
      {/* <FacebookButton
        accessoryLeft={createEvaIcon('facebook', {
          style: { width: 24, height: 24 },
        })}
        onPress={() => {}}
        status="info"
        style={{ marginTop: 8, height: 32, ...generateShadow(6) }}>
        Continuar com o Facebook
      </FacebookButton> */}
      <GoogleButton
        accessoryLeft={createEvaIcon('google', {
          style: { width: 24, height: 24 },
        })}
        onPress={() => {
          signInWithGoogle();
        }}
        status="danger"
        style={{ marginTop: 8, height: 32, ...generateShadow(2) }}>
        Continuar com o Google
      </GoogleButton>
    </>
  );
};
