/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { colors } from '@/constants';
import { isDarkTheme } from '@/utils';

import {
  About,
  AccountSettings,
  ChangePassword,
  ContactSettings,
  ForgetPassword,
  Help,
  Home,
  Login,
  Profile,
  Search,
  SignUp,
  ProductDetails,
  ShoppingCart,
  AddressList,
  EditAddress,
  CreditCardList,
  EditCreditCard,
  OrderPreview,
  OrderList,
  OrderDetails,
} from '@/screens';

import {
  Avatar,
  BottomNavigation,
  BottomNavigationTab,
} from '@ui-kitten/components';
import PaymentMethods from '@/screens/PaymentMethods';
import { navigationRef } from '@/utils/Navigation';

const Stack = createStackNavigator();
const Tabs = createBottomTabNavigator();

const HomeIcon = () => (
  <Avatar
    style={{ height: 24, width: 24 }}
    shape="square"
    source={
      isDarkTheme()
        ? require('@/assets/home_dark.png')
        : require('@/assets/home_light.png')
    }
  />
);

const HomeIconOutline = () => (
  <Avatar
    style={{ height: 24, width: 24 }}
    shape="square"
    source={
      isDarkTheme()
        ? require('@/assets/home-outline_dark.png')
        : require('@/assets/home-outline_light.png')
    }
  />
);

const SearchIcon = () => (
  <Avatar
    style={{ height: 24, width: 24 }}
    shape="square"
    source={
      isDarkTheme()
        ? require('@/assets/search_dark.png')
        : require('@/assets/search_light.png')
    }
  />
);

const SearchIconOutline = () => (
  <Avatar
    style={{ height: 24, width: 24 }}
    shape="square"
    source={
      isDarkTheme()
        ? require('@/assets/search-outline_dark.png')
        : require('@/assets/search-outline_light.png')
    }
  />
);

const UserIcon = () => (
  <Avatar
    style={{ height: 24, width: 24 }}
    shape="square"
    source={
      isDarkTheme()
        ? require('@/assets/user_dark.png')
        : require('@/assets/user_light.png')
    }
  />
);

const UserIconOutline = () => (
  <Avatar
    style={{ height: 24, width: 24 }}
    shape="square"
    source={
      isDarkTheme()
        ? require('@/assets/user-outline_dark.png')
        : require('@/assets/user-outline_light.png')
    }
  />
);

const CartIconOutline = () => (
  <Avatar
    style={{ height: 24, width: 24 }}
    shape="square"
    source={
      isDarkTheme()
        ? require('@/assets/shopping-cart-outline_dark.png')
        : require('@/assets/shopping-cart-outline_light.png')
    }
  />
);

const CartIcon = () => (
  <Avatar
    style={{ height: 24, width: 24 }}
    shape="square"
    source={
      isDarkTheme()
        ? require('@/assets/shopping-cart_dark.png')
        : require('@/assets/shopping-cart_light.png')
    }
  />
);

const BottomTabBar = ({ navigation, state }) => {
  return (
    <BottomNavigation
      style={{
        borderTopWidth: 0.3,
        borderTopColor: colors.divider,
      }}
      indicatorStyle={{
        width: 0,
        height: 0,
      }}
      selectedIndex={state.index}
      onSelect={(index) => {
        navigation.navigate(state.routeNames[index]);
      }}>
      <BottomNavigationTab
        icon={state.index === 0 ? HomeIcon : HomeIconOutline}
        title={''}
      />
      <BottomNavigationTab
        icon={state.index === 1 ? SearchIcon : SearchIconOutline}
        title={''}
      />
      <BottomNavigationTab
        icon={state.index === 2 ? CartIcon : CartIconOutline}
        title={''}
      />
      <BottomNavigationTab
        icon={state.index === 3 ? UserIcon : UserIconOutline}
        title={''}
      />
    </BottomNavigation>
  );
};

const MainTabs = () => (
  <Tabs.Navigator tabBar={(props) => <BottomTabBar {...props} />}>
    <Tabs.Screen name="HomeMenu" component={Home} />
    <Tabs.Screen name="Search" component={Search} />
    <Tabs.Screen name="ShoppingCart" component={ShoppingCart} />
    <Tabs.Screen name="Profile" component={Profile} />
  </Tabs.Navigator>
);

export const AppNavigator = () => (
  // @ts-ignore
  <NavigationContainer ref={navigationRef}>
    <Stack.Navigator headerMode="none">
      <Stack.Screen name="Home" component={MainTabs} />
      <Stack.Screen name="Login" component={Login} />
      <Stack.Screen name="SignUp" component={SignUp} />
      <Stack.Screen name="AccountSettings" component={AccountSettings} />
      <Stack.Screen name="ContactSettings" component={ContactSettings} />
      <Stack.Screen name="Help" component={Help} />
      <Stack.Screen name="AddressList" component={AddressList} />
      <Stack.Screen name="CreditCardList" component={CreditCardList} />
      <Stack.Screen name="EditAddress" component={EditAddress} />
      <Stack.Screen name="EditCreditCard" component={EditCreditCard} />
      <Stack.Screen name="ProductDetails" component={ProductDetails} />
      <Stack.Screen name="About" component={About} />
      <Stack.Screen name="ChangePassword" component={ChangePassword} />
      <Stack.Screen name="ForgetPassword" component={ForgetPassword} />
      <Stack.Screen name="ShoppingCart" component={ShoppingCart} />
      <Stack.Screen name="OrderList" component={OrderList} />
      <Stack.Screen name="PaymentMethods" component={PaymentMethods} />
      <Stack.Screen name="OrderPreview" component={OrderPreview} />
      <Stack.Screen name="OrderDetails" component={OrderDetails} />
    </Stack.Navigator>
  </NavigationContainer>
);
