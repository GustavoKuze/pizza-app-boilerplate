/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { Text, useTheme } from '@ui-kitten/components';
import { Divider } from '@/components';
import { Body } from '@/components/StyledComponents';
import { Product as ProductModel } from '@/models/Product';
import Product from './Product';

// import { Container } from './styles';

type Props = {
  products: ProductModel[];
  name: string;
  navigation: {
    navigate(where: string, params?: any);
    goBack();
  };
};

const MenuSection: React.FC<Props> = ({ products, name, navigation }) => {
  const theme = useTheme();

  const goToProductDetails = async (productFromList: ProductModel) => {
    navigation.navigate('ProductDetails', { productId: productFromList.id });
  };

  return (
    <Body>
      <Text category="h5" style={{ fontWeight: '700' }}>
        {name}
      </Text>
      {products.map((product) => (
        <Product
          key={product.id}
          product={product}
          onPress={() => {
            goToProductDetails(product);
          }}
        />
      ))}
      <Divider style={{ marginTop: 32 }} />
    </Body>
  );
};

export default MenuSection;
