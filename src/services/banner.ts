import { request } from './api';

const getBanners = async (): Promise<any> => {
  try {
    const response = await request('/banner');

    return response.data.data;
  } catch (err) {
    console.log(err);
    return [];
  }
};

export { getBanners };
