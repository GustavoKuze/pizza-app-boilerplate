import { IAPIResponse } from '@/models/IAPIResponse';
import { navigate, goBack } from '@/utils/Navigation';
import { loadState } from '@/utils/asyncStorage';

const request = async (
  url: string,
  method: 'POST' | 'PUT' | 'DELETE' | 'GET' = 'GET',
  body?: object | undefined,
  headers?: HeadersInit_ | undefined,
  supressHeaders: boolean = false,
): Promise<IAPIResponse> => {
  try {
    const loginState = (await loadState('login')) || {};

    const { token } = loginState;
    const authorization = token
      ? { authorization: loginState.uid ? token : `Bearer ${token}` }
      : {};

    // @ts-ignore
    let headersToUse: HeadersInit_ | undefined = headers
      ? headers
      : {
          Accept: 'application/json',
          'Content-Type': 'application/json',
          ...authorization,
        };

    if (supressHeaders) {
      headersToUse = undefined;
    }

    const response = await fetch(`${'http://localhost:5503'}/api${url}`, {
      method,
      body: typeof body === 'object' ? JSON.stringify(body) : body,
      headers: headersToUse,
    });

    if (response.status === 401) {
      // @ts-ignore
      goBack();
      // @ts-ignore
      navigate('Login');
    }

    const data = await response.json();

    return {
      data,
      status: response.status,
      ok: response.ok,
    };
  } catch (err) {
    return {
      data: null,
      ok: false,
      status: 0,
      error: err.message,
    };
  }
};

export { request };
