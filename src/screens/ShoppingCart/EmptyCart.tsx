import React from 'react';
import { Dimensions } from 'react-native';
import { useTheme, Text, Icon } from '@ui-kitten/components';
import { CenteredView } from '@/components/StyledComponents';
import { Button } from '@/components';

// import { Container } from './styles';

type Props = {
  navigation: any;
};

const EmptyCart: React.FC<Props> = ({ navigation }) => {
  const theme = useTheme();
  return (
    <CenteredView>
      <Icon
        fill={theme['text-hint-color']}
        style={{
          width: Dimensions.get('screen').width / 2,
          height: Dimensions.get('screen').width / 2,
          marginTop: '20%',
        }}
        name="shopping-cart-outline"
      />
      <Text
        category="h3"
        style={{ color: theme['text-hint-color'], textAlign: 'center' }}>
        Seu carrinho está vazio
      </Text>
      <Button
        onPress={() => {
          navigation.replace('Home');
        }}
        style={{ marginTop: 58 }}>
        Ver cardápio
      </Button>
    </CenteredView>
  );
};

export default EmptyCart;
