/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */

import React from 'react';
import {
  Dimensions,
  ListRenderItemInfo,
  NativeSyntheticEvent,
  StyleSheet,
  TextInputFocusEventData,
  TextInputSubmitEditingEventData,
  View,
} from 'react-native';
import { ChildrenWithProps } from '@ui-kitten/components/devsupport';

import {
  AutocompleteItemElement,
  AutocompleteItemProps,
  Popover,
  PopoverElement,
  List,
  Input,
  InputElement,
  InputProps,
} from '@ui-kitten/components';
import { generateShadow } from '@/utils';

export interface AutocompleteProps extends InputProps {
  children?: ChildrenWithProps<AutocompleteItemProps>;
  onSelect?: (index: number) => void;
  placement?: string;
}

export type AutocompleteElement = React.ReactElement<AutocompleteProps>;

interface State {
  listVisible: boolean;
}

class Autocomplete extends React.Component<AutocompleteProps, State> {
  public state: State = {
    listVisible: false,
  };

  private popoverRef = React.createRef<Popover>();
  private inputRef = React.createRef<Input>();

  private get data(): any[] {
    return React.Children.toArray(this.props.children || []);
  }

  public show = (): void => {
    this.popoverRef.current?.show();
  };

  public hide = (): void => {
    this.popoverRef.current?.hide();
  };

  public focus = (): void => {
    this.inputRef.current?.focus();
  };

  public blur = (): void => {
    this.inputRef.current?.blur();
  };

  public isFocused = (): boolean => {
    return this.inputRef.current?.isFocused();
  };

  public clear = (): void => {
    this.inputRef.current?.clear();
  };

  private onInputFocus = (
    event: NativeSyntheticEvent<TextInputFocusEventData>,
  ): void => {
    this.setOptionsListVisible();
    this.props.onFocus && this.props.onFocus(event);
  };

  private onInputSubmitEditing = (
    e: NativeSyntheticEvent<TextInputSubmitEditingEventData>,
  ): void => {
    this.setOptionsListInvisible();
    this.props.onSubmitEditing && this.props.onSubmitEditing(e);
  };

  private onBackdropPress = (): void => {
    this.blur();
    this.setOptionsListInvisible();
  };

  private onItemPress = (index: number): void => {
    if (this.props.onSelect) {
      this.setOptionsListInvisible();
      this.props.onSelect(index);
    }
  };

  private setOptionsListVisible = (): void => {
    const hasData: boolean = this.data.length > 0;
    hasData && this.setState({ listVisible: true });
  };

  private setOptionsListInvisible = (): void => {
    this.setState({ listVisible: false });
  };

  private renderItem = (
    info: ListRenderItemInfo<AutocompleteItemElement>,
  ): AutocompleteItemElement => {
    return React.cloneElement(info.item, {
      onPress: () => this.onItemPress(info.index),
    });
  };

  private renderInputElement = (props: InputProps): InputElement => {
    return (
      <View>
        <Input
          {...props}
          ref={this.inputRef}
          onFocus={this.onInputFocus}
          onSubmitEditing={this.onInputSubmitEditing}
        />
      </View>
    );
  };

  public render(): PopoverElement {
    const { placement, children, ...inputProps } = this.props;

    return (
      <Popover
        ref={this.popoverRef}
        style={styles.popover}
        placement={placement}
        visible={this.state.listVisible}
        fullWidth={true}
        anchor={() => this.renderInputElement(inputProps)}
        onBackdropPress={this.onBackdropPress}>
        <List
          style={styles.list}
          data={this.data}
          bounces={false}
          renderItem={this.renderItem}
        />
      </Popover>
    );
  }
}

export default Autocomplete;

const styles = StyleSheet.create({
  popover: {
    maxHeight: Dimensions.get('window').height / 1.8,
    overflow: 'hidden',
    ...generateShadow(8),
  },
  list: {
    flex: 1,
    height: Dimensions.get('window').height,
    overflow: 'hidden',
  },
});
