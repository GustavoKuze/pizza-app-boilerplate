import { Avatar } from '@ui-kitten/components';
import styled from 'styled-components/native';

export const NoResultsImage = styled(Avatar)`
  height: ${150}px;
  width: ${150}px;
`;
