import { IAddress } from '@/models/IAddress';
import { ICartItem } from '@/models/ICartItem';
import {
  ICart,
  ICartDeliveryFee,
  ICartDiscount,
  ICartPayment,
  ICartUser,
} from '@/models/ICart';
import { saveState, deleteState } from '@/utils/asyncStorage';

const typesPrefix = '@cart';

export const types = {
  SET_LOADING: `${typesPrefix}/SET_LOADING`,
  ADD_CART_ITEM: `${typesPrefix}/ADD_CART_ITEM`,
  UPDATE_CART_ITEM: `${typesPrefix}/UPDATE_CART_ITEM`,
  REMOVE_CART_ITEM: `${typesPrefix}/REMOVE_CART_ITEM`,
  SET_PAYMENT_METHOD: `${typesPrefix}/SET_PAYMENT_METHOD`,
  SET_DELIVERY_FEE: `${typesPrefix}/SET_DELIVERY_FEE`,
  ADD_COUPON: `${typesPrefix}/ADD_COUPON`,
  SET_ADDRESS: `${typesPrefix}/SET_ADDRESS`,
  SET_USER: `${typesPrefix}/SET_USER`,
  RESET_CART: `${typesPrefix}/RESET_CART`,
  LOAD_CART: `${typesPrefix}/LOAD_CART`,
};

const INITIAL_STATE: {
  cart: ICart;
  isLoading: Boolean;
} = {
  cart: {
    // @ts-ignore
    address: {},
    items: [],
    // @ts-ignore
    payment: {},
    // @ts-ignore
    user: {},
    // @ts-ignore
    coupons: [],
    // @ts-ignore
    deliveryFee: {},
    status: 'aguardando_pagamento',
  },
  isLoading: false,
};

export default (state = INITIAL_STATE, action: any) => {
  let cart = INITIAL_STATE;

  switch (action.type) {
    case types.ADD_CART_ITEM:
      cart = {
        ...state,
        cart: { ...state.cart, items: [...state.cart.items, action.payload] },
      };
      saveState(cart, 'cart');
      return cart;
    case types.UPDATE_CART_ITEM:
      cart = {
        ...state,
        cart: {
          ...state.cart,
          items: state.cart.items.map((item) =>
            item.id === action.payload.id ? action.payload : item,
          ),
        },
      };
      saveState(cart, 'cart');
      return cart;
    case types.REMOVE_CART_ITEM:
      cart = {
        ...state,
        cart: {
          ...state.cart,
          items: state.cart.items.filter(
            (item) => item.id !== action.payload.id,
          ),
        },
      };
      saveState(cart, 'cart');
      return cart;
    case types.SET_PAYMENT_METHOD:
      cart = {
        ...state,
        cart: {
          ...state.cart,
          payment: action.payload,
        },
      };
      saveState(cart, 'cart');
      return cart;
    case types.SET_DELIVERY_FEE:
      cart = {
        ...state,
        cart: {
          ...state.cart,
          deliveryFee: action.payload,
        },
      };
      saveState(cart, 'cart');
      return cart;
    case types.ADD_COUPON:
      cart = {
        ...state,
        cart: {
          ...state.cart,
          coupons: [...(state.cart.coupons || []), action.payload],
        },
      };
      saveState(cart, 'cart');
      return cart;
    case types.SET_ADDRESS:
      cart = {
        ...state,
        cart: {
          ...state.cart,
          address: action.payload,
        },
      };
      saveState(cart, 'cart');
      return cart;
    case types.SET_USER:
      cart = {
        ...state,
        cart: {
          ...state.cart,
          user: action.payload,
        },
      };
      saveState(cart, 'cart');
      return cart;
    case types.SET_LOADING:
      cart = { ...state, isLoading: action.payload };
      saveState(cart, 'cart');
      return cart;
    case types.RESET_CART:
      cart = INITIAL_STATE;
      deleteState('cart');
      return cart;
    case types.LOAD_CART:
      cart = action.payload || INITIAL_STATE;
      return cart;
    default:
      return state;
  }
};

export const addCartItem = (cartItem: ICartItem) => ({
  type: types.ADD_CART_ITEM,
  payload: cartItem,
});

export const updateCartItem = (cartItem: ICartItem) => ({
  type: types.UPDATE_CART_ITEM,
  payload: cartItem,
});

export const removeCartItem = (cartItem: ICartItem) => ({
  type: types.REMOVE_CART_ITEM,
  payload: cartItem,
});

export const setPaymentMethod = (payment: ICartPayment) => ({
  type: types.SET_PAYMENT_METHOD,
  payload: payment,
});

export const setDeliveryFee = (deliveryFee: ICartDeliveryFee) => ({
  type: types.SET_DELIVERY_FEE,
  payload: deliveryFee,
});

export const setAddress = (address: IAddress) => ({
  type: types.SET_ADDRESS,
  payload: address,
});

export const setUser = (user: ICartUser) => ({
  type: types.SET_USER,
  payload: user,
});

export const setIsLoading = (isLoading: Boolean) => ({
  type: types.SET_LOADING,
  payload: isLoading,
});

export const addCoupon = (coupon: ICartDiscount) => ({
  type: types.ADD_COUPON,
  payload: coupon,
});

export const loadCart = (cart: ICart) => ({
  type: types.LOAD_CART,
  payload: cart,
});

export const allActions = {
  addCartItem,
  updateCartItem,
  removeCartItem,
  setPaymentMethod,
  setDeliveryFee,
  setAddress,
  setUser,
  setIsLoading,
  addCoupon,
  loadCart,
};
