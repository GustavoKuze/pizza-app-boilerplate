export interface IModalContent {
  title?: string | React.FC | Element;
  message: string | React.FC | Element;
  buttons?: any[] | undefined | null;
  status?: 'danger' | 'success' | 'info' | 'warning' | undefined;
  isOpen?: Boolean | undefined;
  showCloseIcon?: Boolean | undefined;
  noBackdrop?: Boolean | undefined;
  closable?: Boolean | undefined;
}
