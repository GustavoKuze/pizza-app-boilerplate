import React, { useEffect, useState } from 'react';
import {
  ActivityIndicator,
  Dimensions,
  SafeAreaView,
  ScrollView,
} from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles from './styles';
import { Divider, useTheme, Text, Layout } from '@ui-kitten/components';
import { Price, TitleBar, Product } from '@/components';
import { Body, SpaceBetweenView } from '@sc/index';
import { Order } from '@/models/Order';
import { getOrderDetails } from '@/services/order';
import { paymentMethodsFriendlyLabels } from '@/constants';

type RootStackParamList = {
  OrderDetails: { orderId: number };
  ProductDetails: { productId: number };
  Home: undefined;
};

type Props = {
  route: RouteProp<RootStackParamList, 'OrderDetails'>;
  navigation: StackNavigationProp<RootStackParamList, 'OrderDetails'>;
};

export const OrderDetails: React.FC<Props> = ({ route, navigation }) => {
  const theme = useTheme();
  // @ts-ignore
  const [order, setOrder] = useState<Order>({});
  const [isLoadingOrder, setIsLoadingOrder] = useState(false);

  useEffect(() => {
    if (route.params.orderId) {
      (async () => {
        setIsLoadingOrder(true);
        const order = await getOrderDetails(route.params.orderId);

        setOrder(order);
        setIsLoadingOrder(false);
      })();
    }
  }, [route.params.orderId]);

  return (
    <SafeAreaView
      style={{
        ...styles.container,
        backgroundColor: theme['background-basic-color-1'],
      }}>
      <TitleBar
        title={'Detalhes do pedido'}
        onGoBack={() => navigation.goBack()}
      />
      <Divider />

      <ScrollView>
        <Body style={{ paddingBottom: 8 }}>
          {isLoadingOrder ? (
            <ActivityIndicator
              style={{ marginTop: Dimensions.get('screen').height / 3 }}
              size={64}
              color={theme['color-primary-500']}
            />
          ) : (
            <>
              <SpaceBetweenView row>
                <Layout>
                  <Text category="p1" style={{ fontWeight: 'bold' }}>
                    Número do pedido:
                  </Text>
                  <Text
                    category="label"
                    style={{ fontSize: 14, marginBottom: 32 }}>
                    #{order.id}
                  </Text>
                </Layout>
                <Layout>
                  <Text category="p1">{order.date}</Text>
                  <Price
                    value={order.totalPrice}
                    style={{ fontSize: 20, marginLeft: 'auto' }}
                  />
                </Layout>
              </SpaceBetweenView>
              <Text category="p1" style={{ fontWeight: 'bold' }}>
                Situação do pedido:
              </Text>
              <Text category="label" style={{ fontSize: 14, marginBottom: 32 }}>
                {order.status}
              </Text>

              <Divider style={{ marginBottom: 32 }} />

              {order.items &&
                order.items.length &&
                order.items.map((item) => (
                  <>
                    <Product
                      key={item.id}
                      onPress={() => {
                        navigation.navigate('ProductDetails', {
                          // @ts-ignore
                          productId: item.productId,
                        });
                      }}
                      product={item}
                    />
                    <Divider style={{ marginVertical: 8 }} />
                  </>
                ))}

              <Divider style={{ marginVertical: 16 }} />

              {order.address ? (
                <>
                  <Text category="p1" style={{ fontWeight: 'bold' }}>
                    Endereço:
                  </Text>
                  <Text category="label" style={{ fontSize: 14 }}>
                    {order.address.name}
                  </Text>
                  <Text category="label" style={{ fontSize: 14 }}>
                    {order.address.street}
                  </Text>
                  <Text category="label" style={{ fontSize: 14 }}>
                    Nº {order.address.number}
                  </Text>
                  <Text category="label" style={{ fontSize: 14 }}>
                    {order.address.neighborhood}
                  </Text>
                  <Text category="label" style={{ fontSize: 14 }}>
                    {order.address.city}
                  </Text>

                  <Divider style={{ marginVertical: 16 }} />
                </>
              ) : undefined}
              {order.payment ? (
                <>
                  <Text category="p1" style={{ fontWeight: 'bold' }}>
                    Forma de pagamento:
                  </Text>
                  <Text category="label" style={{ fontSize: 14 }}>
                    {paymentMethodsFriendlyLabels[order.payment.name]}
                  </Text>
                  {order.payment.card ? (
                    <>
                      <Text category="label" style={{ fontSize: 14 }}>
                        {`**** **** **** ${
                          order.payment.card.number.split(' ')[3]
                        }`}
                      </Text>
                      <Text category="label" style={{ fontSize: 14 }}>
                        {order.payment.card.name}
                      </Text>
                      <Text category="label" style={{ fontSize: 14 }}>
                        {order.payment.card.expiration}
                      </Text>
                    </>
                  ) : undefined}

                  <Divider style={{ marginTop: 16 }} />
                </>
              ) : undefined}

              {order.coupons && order.coupons.length > 0 ? (
                <>
                  <Text category="p1" style={{ fontWeight: 'bold' }}>
                    Cupons Aplicados:
                  </Text>
                  {order.coupons.map((coupon) => (
                    <Text category="label" style={{ fontSize: 14 }}>
                      {`- #${coupon.code} (${coupon.discountPercentage}%)`}
                    </Text>
                  ))}

                  <Divider style={{ marginVertical: 16 }} />
                </>
              ) : undefined}

              <Divider style={{ marginVertical: 16 }} />
            </>
          )}
        </Body>
      </ScrollView>
    </SafeAreaView>
  );
};
