import { request } from './api';

const getMenu = async (): Promise<any> => {
  try {
    const response = await request('/menu/1');

    return response.data.data;
  } catch (err) {
    return { error: err.message };
  }
};

const getProductDetails = async (productId: number): Promise<any> => {
  try {
    const response = await request(`/menu/product/${productId}`);

    return response.data.data;
  } catch (err) {
    console.log(err);
    return [];
  }
};

export { getMenu, getProductDetails };
