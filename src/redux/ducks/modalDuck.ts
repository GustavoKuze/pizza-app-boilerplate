import { IModalContent } from '../..//models/IModalContent';

const typesPrefix = '@modal';

export const types = {
  SET_MODAL_CONTENT: `${typesPrefix}/SET_MODAL_CONTENT`,
  SET_IS_MODAL: `${typesPrefix}/SET_IS_MODAL`,
};

const INITIAL_STATE = {
  title: '',
  message: '',
  buttons: null,
  status: undefined,
  isOpen: false,
  showCloseIcon: false,
  noBackdrop: false,
  closable: true,
};

export default (state = INITIAL_STATE, action: any) => {
  switch (action.type) {
    case types.SET_IS_MODAL:
      return { ...state, isOpen: action.payload };
    case types.SET_MODAL_CONTENT:
      return {
        ...state,
        ...action.payload,
        title:
          typeof action.payload.title === 'undefined'
            ? INITIAL_STATE.title
            : action.payload.title,
        buttons:
          typeof action.payload.buttons === 'undefined'
            ? INITIAL_STATE.buttons
            : action.payload.buttons,
        status:
          typeof action.payload.status === 'undefined'
            ? INITIAL_STATE.status
            : action.payload.status,
        isOpen:
          typeof action.payload.isOpen === 'undefined'
            ? INITIAL_STATE.isOpen
            : action.payload.isOpen,
        showCloseIcon:
          typeof action.payload.showCloseIcon === 'undefined'
            ? INITIAL_STATE.showCloseIcon
            : action.payload.showCloseIcon,
        noBackdrop:
          typeof action.payload.noBackdrop === 'undefined'
            ? INITIAL_STATE.noBackdrop
            : action.payload.noBackdrop,
        closable:
          typeof action.payload.closable === 'undefined'
            ? INITIAL_STATE.closable
            : action.payload.closable,
      };
    default:
      return state;
  }
};

export const setIsModalOpen = (isOpen) => ({
  type: types.SET_IS_MODAL,
  payload: isOpen,
});

export const setModalContent = (modalContent: IModalContent) => ({
  type: types.SET_MODAL_CONTENT,
  payload: modalContent,
});

export const allActions = {
  setIsModalOpen,
  setModalContent,
};
