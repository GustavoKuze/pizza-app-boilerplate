import { IAddress } from './IAddress';
import { ICartItem } from './ICartItem';
import { ICreditCard } from './ICreditCard';

export interface ICartUser {
  id: number;
  name: string;
  email: string;
  username?: string;
  phone?: string;
}

export interface ICartPayment {
  id: number;
  name: string;
  card?: ICreditCard | null | undefined;
}

export interface ICartDiscount {
  id: number;
  code: string;
  discountPercentage: number;
}

export interface ICartDeliveryFee {
  id: number;
  value: number;
}

export interface ICart {
  user: ICartUser;
  address: IAddress;
  payment: ICartPayment;
  status?: string;
  items: ICartItem[];
  coupons?: ICartDiscount[];
  deliveryFee?: ICartDeliveryFee | undefined;
}
