import { Card, Modal as UIKittenModal, Text } from '@ui-kitten/components';
import { Button } from '@/components/Button';
import React from 'react';
import { View } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { createEvaIcon } from '@/utils/layout';

import { setIsModalOpen } from '@/redux/ducks/modalDuck';

import styles from './styles';

export const Modal: React.FC = () => {
  const dispatch = useDispatch();
  const {
    title,
    message,
    buttons,
    status,
    isOpen,
    showCloseIcon,
    noBackdrop,
    closable,
  } = useSelector((state: any) => state.modal);

  const callSetIsModalOpen = (shouldOpen) => {
    dispatch(setIsModalOpen(shouldOpen));
  };

  const renderButtons = () => {
    if (buttons && buttons.length > 0) {
      return (
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            alignItems: 'center',
            marginTop: 16,
            marginBottom: 8,
          }}>
          {buttons}
        </View>
      );
    }
    return (
      <Button
        style={styles.btnOk}
        status={status}
        onPress={() => callSetIsModalOpen(false)}>
        OK
      </Button>
    );
  };

  const renderMessage = () => {
    if (!message) {
      return undefined;
    }
    if (typeof message === 'string') {
      return <Text style={styles.modalMessage}>{message}</Text>;
    }
    return <>{message}</>;
  };

  const renderTitle = () => {
    if (!title) {
      return undefined;
    }
    if (typeof message === 'string') {
      return (
        <Text category="h4" style={styles.modalTitle}>
          {title}
        </Text>
      );
    }
    return <>{title}</>;
  };

  return (
    <View style={styles.container}>
      <UIKittenModal
        visible={isOpen}
        backdropStyle={noBackdrop ? undefined : styles.backdrop}
        onBackdropPress={() =>
          noBackdrop || !closable ? undefined : callSetIsModalOpen(false)
        }>
        <Card style={styles.modalBody} status={status}>
          {showCloseIcon ? (
            <Button
              appearance="ghost"
              status="basic"
              style={styles.button}
              accessoryLeft={createEvaIcon('close-outline', {
                style: styles.closeIcon,
              })}
              onPress={() => {
                callSetIsModalOpen(false);
              }}
            />
          ) : undefined}

          {renderTitle()}
          {renderMessage()}
          {renderButtons()}
        </Card>
      </UIKittenModal>
    </View>
  );
};
