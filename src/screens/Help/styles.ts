import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  select: {
    margin: 2,
    marginTop: 16,
  },
});

export default styles;
