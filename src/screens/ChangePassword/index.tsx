import React, { useRef, useState } from 'react';
import {
  SafeAreaView,
  ToastAndroid,
  TouchableWithoutFeedback,
} from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';
import styles from './styles';
import { TitleBar, Button } from '@/components';
import { Icon, Input } from '@ui-kitten/components';
import { Body } from '@sc/index';

type RootStackParamList = {
  ChangePassword: undefined;
  Settings: undefined;
};

type Props = {
  route: RouteProp<RootStackParamList, 'ChangePassword'>;
  navigation: StackNavigationProp<RootStackParamList, 'ChangePassword'>;
};

export const ChangePassword: React.FC<Props> = ({ navigation }) => {
  const [secureTextEntry, setSecureTextEntry] = React.useState(true);
  const [currentPassword, setCurrentPassword] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [confirmNewPassword, setConfirmNewPassword] = useState('');

  const [currentPasswordInputStatus, setCurrentPasswordInputStatus] =
    useState('error');

  const [newPasswordInputStatus, setNewPasswordInputStatus] = useState('error');

  const [confirmNewPasswordInputStatus, setConfirmNewPasswordInputStatus] =
    useState('error');

  const currentPasswordInput = useRef<any>();
  const newPasswordInput = useRef<any>();
  const confirmNewPasswordInput = useRef<any>();

  const validateFields = (): Boolean => {
    setCurrentPasswordInputStatus(currentPassword ? 'success' : 'danger');
    setNewPasswordInputStatus(newPassword ? 'success' : 'danger');
    setConfirmNewPasswordInputStatus(confirmNewPassword ? 'success' : 'danger');

    return !!currentPassword && !!newPassword && !!confirmNewPassword;
  };

  const toggleSecureEntry = () => {
    setSecureTextEntry(!secureTextEntry);
  };

  const renderIcon = (props) => (
    <TouchableWithoutFeedback onPress={toggleSecureEntry}>
      <Icon {...props} name={secureTextEntry ? 'eye-off' : 'eye'} />
    </TouchableWithoutFeedback>
  );

  const saveNewPassword = () => {
    if (!validateFields()) {
      return;
    }

    ToastAndroid.show('Nova senha salva', ToastAndroid.SHORT);
    navigation.goBack();
  };

  return (
    <SafeAreaView style={styles.container}>
      <TitleBar title={'Mudar senha'} onGoBack={() => navigation.goBack()} />
      <Body>
        <Input
          ref={currentPasswordInput}
          placeholder="Digite sua senha atual"
          label="Senha atual"
          accessoryRight={renderIcon}
          secureTextEntry={secureTextEntry}
          value={currentPassword}
          onChangeText={(nextValue) => setCurrentPassword(nextValue)}
          status={currentPasswordInputStatus}
          onBlur={validateFields}
          onSubmitEditing={() => {
            newPasswordInput.current.focus();
          }}
        />
        <Input
          ref={newPasswordInput}
          placeholder="Digite sua nova senha"
          label="Nova senha"
          accessoryRight={renderIcon}
          secureTextEntry={secureTextEntry}
          value={newPassword}
          onChangeText={(nextValue) => setNewPassword(nextValue)}
          status={newPasswordInputStatus}
          onBlur={validateFields}
          onSubmitEditing={() => {
            confirmNewPasswordInput.current.focus();
          }}
        />
        <Input
          ref={confirmNewPasswordInput}
          placeholder="Repita a nova senha"
          label="Confirmar senha"
          accessoryRight={renderIcon}
          secureTextEntry={secureTextEntry}
          value={confirmNewPassword}
          onChangeText={(nextValue) => setConfirmNewPassword(nextValue)}
          status={confirmNewPasswordInputStatus}
          onBlur={validateFields}
          onSubmitEditing={() => {
            saveNewPassword();
          }}
        />
        <Button
          onPress={() => {
            saveNewPassword();
          }}
          status="info"
          style={{ marginTop: 16, marginHorizontal: 58 }}>
          Confirmar
        </Button>
      </Body>
    </SafeAreaView>
  );
};
