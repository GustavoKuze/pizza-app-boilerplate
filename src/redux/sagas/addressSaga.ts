import { put, call } from 'redux-saga/effects';
import { listUserAddresses } from '@/services/address';
import { types as addressTypes } from '@/redux/ducks/addressDuck';

function* fetchAddresses(action: any) {
  yield put({
    type: addressTypes.SET_LOADING,
    payload: true,
  });

  const { userId, onSuccess, onError } = action.payload;

  const result = yield call(() => listUserAddresses(userId));

  if (!result.error && !result.status) {
    yield put({
      type: addressTypes.SET_ADDRESSES,
      payload: result,
    });
    if (typeof onSuccess === 'function') {
      onSuccess();
    }
  } else {
    if (typeof onError === 'function') {
      onError(result);
    }
  }

  yield put({
    type: addressTypes.SET_LOADING,
    payload: false,
  });
}

export { fetchAddresses };
