import { put, call } from 'redux-saga/effects';
import { getMenu } from '@/services/menu';
import { types as menuTypes } from '@/redux/ducks/menuDuck';

function* fetchMenu(action: any) {
  yield put({
    type: menuTypes.SET_LOADING,
    payload: true,
  });

  const { onSuccess, onError } = action.payload;

  const result = yield call(() => getMenu());

  if (!result.error) {
    yield put({
      type: menuTypes.SET_CATEGORIES,
      payload: result.categories,
    });
    if (typeof onSuccess === 'function') {
      onSuccess();
    }
  } else {
    if (typeof onError === 'function') {
      onError(result.error);
    }
  }

  yield put({
    type: menuTypes.SET_LOADING,
    payload: false,
  });
}

export { fetchMenu };
