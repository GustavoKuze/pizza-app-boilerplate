/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect } from 'react';

import { FlatList, StyleSheet, View } from 'react-native';
import { Divider } from '@/components/Divider';
import GenericSkeleton from './GenericSkeleton';

interface Props {
  data: any[];
  loading: Boolean;
  loadData(): void;
  refreshData?(): void;
  noRefresh?: Boolean;
  noDivider?: Boolean;
  renderItem(item: any, index?: number): Element;
  renderSkeleton?(): Element;
  style?: any;
  skeletonsCount?: number;
  shouldRenderDivider: Boolean;
}

export const InfiniteScroll: React.FC<Props> = ({
  data,
  renderItem,
  loadData,
  refreshData,
  loading,
  renderSkeleton,
  style = {},
  skeletonsCount = 10,
  shouldRenderDivider = true,
  noRefresh = false,
  noDivider = false,
}) => {
  useEffect(() => {
    loadData();
  }, []);

  const renderItemComponent = ({ item: dataItem, index }) => (
    <View key={`${dataItem.id}`}>
      {renderItem(dataItem, index)}
      {!!shouldRenderDivider}
      {!noDivider ? <Divider /> : undefined}
    </View>
  );

  const renderSkeletons = () => {
    let skeletons: any[] = [];

    for (let i = 0; i < skeletonsCount; i++) {
      skeletons = [
        ...skeletons,
        <View key={`skeleton-${i}`}>
          {renderSkeleton ? renderSkeleton() : <GenericSkeleton />}
        </View>,
      ];
    }

    return skeletons;
  };

  const renderFooter = () => {
    if (!loading) {
      return null;
    }

    return <>{renderSkeletons()}</>;
  };

  const callLoad = () => {
    setTimeout(() => {
      loadData();
    }, 300);
  };

  const callRefresh = () => {
    setTimeout(() => {
      const fn = refreshData || loadData;
      fn();
    }, 300);
  };

  return (
    <FlatList
      contentContainerStyle={{ ...innerStyles.list, ...style }}
      data={data}
      renderItem={renderItemComponent}
      keyExtractor={(dataItem) => `${dataItem.id}`}
      refreshing={false}
      onEndReached={!noRefresh ? callLoad : () => {}}
      onRefresh={!noRefresh ? callRefresh : () => {}}
      onEndReachedThreshold={0.01}
      ListFooterComponent={renderFooter}
      initialNumToRender={40}
      maxToRenderPerBatch={40}
    />
  );
};

const innerStyles = StyleSheet.create({
  list: {
    
    paddingTop: 8,
  },
});
