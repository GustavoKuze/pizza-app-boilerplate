import React from 'react';
import { Icon } from '@ui-kitten/components';

const createEvaIcon = (iconName: string, extraProps: any = {}) => {
  return (props) => (
    <Icon
      {...props}
      name={iconName}
      {...extraProps}
      style={{ ...props.style, ...extraProps.style }}
    />
  );
};

export { createEvaIcon };
