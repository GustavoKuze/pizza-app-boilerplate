import { combineReducers } from 'redux';
import creditCard from './creditCardDuck';
import address from './addressDuck';
import order from './orderDuck';
import cart from './cartDuck';
import menu from './menuDuck';
import search from './searchDuck';
import user from './userDuck';
import modal from './modalDuck';

export default combineReducers({
  creditCard,
  address,
  order,
  cart,
  menu,
  modal,
  search,
  user,
});
