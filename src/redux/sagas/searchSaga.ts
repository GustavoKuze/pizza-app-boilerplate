import { put, call } from 'redux-saga/effects';
import { searchProducts } from '@/services/search';
import { types as searchTypes } from '@/redux/ducks/searchDuck';

function* searchProductSaga(action: any) {
  yield put({
    type: searchTypes.SET_LOADING,
    payload: true,
  });

  const result = yield call(() => searchProducts(action.payload));

  yield put({
    type: searchTypes.SET_SEARCH_RESULT,
    payload: result,
  });

  yield put({
    type: searchTypes.SET_LOADING,
    payload: false,
  });
}

export { searchProductSaga };
