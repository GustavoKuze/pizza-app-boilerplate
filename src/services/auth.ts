import { ILoginResult } from '@/models/ILoginResult';
import { ISignUpResult } from '@/models/ISignUpResult';
import { request } from './api';

const login = async (
  email: string,
  password: string,
): Promise<ILoginResult> => {
  try {
    const response = await request('/user/login', 'POST', {
      email,
      password,
    });

    return response.data;
  } catch (err) {
    console.log(err);
    return {};
  }
};

const signUp = async (
  email: string,
  password: string,
  name: string,
): Promise<ISignUpResult> => {
  try {
    const response = await request('/user', 'POST', {
      name,
      email,
      password,
    });

    return response.data.data;
  } catch (err) {
    console.log(err);
    return {};
  }
};

const socialLogin = async (
  uid: string,
  email: string,
  provider: string,
  displayName: string,
): Promise<any> => {
  try {
    const response = await request('/user/social-login', 'POST', {
      uid,
      email,
      provider,
      displayName,
    });

    return response.data;
  } catch (err) {
    console.log(err);
    return {};
  }
};

export { login, signUp, socialLogin };
