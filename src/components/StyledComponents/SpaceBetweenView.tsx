import { View } from 'react-native';
import styled from 'styled-components/native';

export const SpaceBetweenView = styled(View)`
  flex-direction: ${({ row }) => (row ? 'row' : 'column')};
  align-items: center;
  justify-content: space-between;
  ${({ extraStyles }) => (extraStyles ? extraStyles : '')}
`;

export default SpaceBetweenView;
